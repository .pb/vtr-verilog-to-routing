#ifndef PLASMA_BYTECODE_H
#define PLASMA_BYTECODE_H

#include <cstdio>
#include <string>
#include <capnp/common.h>
#include <capnp/serialize-packed.h>
#include <kj/memory.h>

#include "garble.capnp.h"

namespace plasma {
    class Bytecode {
    public:
        Bytecode(const std::string& fileName);
        ~Bytecode();

        ::garble::Executable::Reader operator()() const;

        ::garble::InstructionSegment::Reader getSegment(const std::string& name);

    protected:
        static FILE* loadFp(const std::string& fileName);
        void processBytecode();

        FILE* fp_;
        ::capnp::StreamFdMessageReader bcreader_;
        ::garble::Executable::Reader rootChecked_;
        ::kj::Array<::capnp::word> bcBuffer_;
        ::garble::Executable::Reader root_;
    };
}

#endif
