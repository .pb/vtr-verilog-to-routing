#include "memory_space.h"
#include "bytecode_util.h"

#include <cstdio>


namespace plasma {
    using libvcd::VVal;

    MemorySpace::MemorySpace(Bytecode& bc):
        bc_(bc),
        dspec_(bc_().getDSpec()),
        memA_(dspec_.getNetCount(), VVal::X),
        memB_(dspec_.getNetCount(), VVal::X),
        omega_(&memA_),
        delta_(&memB_),
        omega_barrier_(dspec_.getOmegaBarrier()),
        net_lookup_(),
        net_name_lookup_() {
        for (garble::NetMap::Reader m : dspec_.getNamedNetMap()) {
            net_lookup_.emplace(m.getName(), m.getAddr());
            net_name_lookup_.emplace(m.getAddr(), m.getName());
        }

        initialiseSpace();
    }

    void MemorySpace::initialiseSpace() {
        for (garble::DataSpaceInit::Reader init : bc_().getDInit()) {
            DAddr addr = init.getAddr();
            int count = init.getCount();
            VVal val = netstate_to_vval(init.getVal());

            while (count--) {
                (*delta_)[addr] = val;
                (*omega_)[addr++] = val;
            }
        }
    }

    libvcd::VVal& MemorySpace::omega(DAddr idx, bool barrier) {
        // FIXME: This should be disabled really, we can do this check statically
        if (barrier && idx > omega_barrier_)
            throw OmegaBarrierViolation(idx, barrier);

        return (*omega_)[idx];
    }

    libvcd::VVal& MemorySpace::delta(DAddr idx) {
        return (*delta_)[idx];
    }

    MemorySpace::DAddr MemorySpace::lookupAddr(const std::string& netName) const {
        auto it = net_lookup_.find(netName);

        if (it == net_lookup_.end())
            throw std::runtime_error("Referenced net not in bytecode");

        return it->second;
    }

    const std::string MemorySpace::lookupName(DAddr addr, bool* success) const {
        auto it = net_name_lookup_.find(addr);

        if (it == net_name_lookup_.end()) {
            if (success != nullptr)
                *success = false;

            return "??unknown??";
        }

        if (success != nullptr)
            *success = true;

        return it->second;
    }

    void MemorySpace::swap() {
        std::swap(omega_, delta_);
    }

    OmegaBarrierViolation::OmegaBarrierViolation(MemorySpace::DAddr operand,
                                                    MemorySpace::DAddr barrier):
        std::domain_error(""), op_(operand), barrier_(barrier) {
    }

    const char* OmegaBarrierViolation::what() {
        snprintf(whatBuffer_, sizeof(whatBuffer_),
                "An omega barrier violation occurred: 0x%x > 0x%x",
                op_, barrier_);
        return whatBuffer_;
    }
}
