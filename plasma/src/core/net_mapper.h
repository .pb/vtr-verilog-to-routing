#ifndef PLASMA_NET_MAPPER_H
#define PLASMA_NET_MAPPER_H

#include <vector>

#include "libvcd_vcd.h"
#include "memory_space.h"

namespace plasma {
    class NetMapper {
    public:
        typedef std::vector<std::pair<libvcd::net::id_t, MemorySpace::DAddr>>
                net_id_map;

    protected:
        net_id_map net_lookup_;
    };
}

#endif
