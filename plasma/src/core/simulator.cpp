#include "simulator.h"
#include "scopetimer.h"
#include "libvcd_vval.h"

namespace plasma {
    using libvcd::VVal;

    Simulator::Simulator(Bytecode& bc,
                            MemorySpace& mem,
                            InputManager& in,
                            OutputManager& out):
        bc_(bc), mem_(mem), in_(in), out_(out) {
    }

    int Simulator::run(const SegmentSchedule& schedule, int num_cycles, int t) {
        scopetimer timer(lastTime_);

        garble::InstructionSegment::Reader seg = bc_.getSegment(schedule[0].second);

        int next_segment = 1;
        int sched_size = schedule.size();
        int next_at = sched_size > 1 ? schedule[1].first : -1;

        auto instrList = seg.getIList();
        int segSize = instrList.size();

        out_.flushMem(t);

        // In each clock cycle:
        do {
            // Read inputs into delta space
            in_.readInputs(t);

            // Now, execute the program, one instruction at a time
            for (int pc = 0; pc < segSize; pc++) {
                ::garble::Instruction::Reader i = instrList[pc];

                VVal result;

                switch(i.getOp().which()) {
                    /* FIXME: make this a dispatch functor lookup to enable addition
                     * of new opcodes without code changes here */
                    case ::garble::Instruction::Op::MOV_O:
                        result = exec(i.getOp().getMovO());
                        break;

                    case ::garble::Instruction::Op::LUT:
                        result = exec(i.getOp().getLut());
                        break;

                    default:
                        throw std::runtime_error("Invalid instruction");
                }

                /* FIXME: We can actually bin the destination field if we want
                 * (as all nets have 1 driver, PC == NetId!!) */
                mem_.delta(i.getDestD()) = result;
            }

            // Write outputs from delta space
            out_.writeOutputs(t);

            // Swap the delta and omega spaces
            mem_.swap();

            // Patch in the next segment if necessary
            if (t == next_at) {
                const std::string& next_seg = schedule[next_segment].second;
                std::cout << "i Patched segment `" << next_seg
                            << "` at t=" << t << std::endl;
                seg = bc_.getSegment(next_seg);
                instrList = seg.getIList();
                segSize = instrList.size();
                next_segment++;
                next_at = next_segment >= sched_size ? -1 : next_segment;
            }
        } while (t++ < num_cycles);

        return lastCycles_ = t - 1;
    }

    VVal Simulator::exec(::garble::InstrMovOmega::Reader op) {
        return mem_.omega(op.getSrc());
    }

    VVal Simulator::exec(::garble::InstrLut::Reader op) {
        unsigned int selector = 0;
        auto srcAddrs = op.getSrcDs();
        int k = srcAddrs.size(); // FIXME: Precompute this and store in lookup?

        for (int input_idx = 0; input_idx < k; input_idx++) {
            switch (mem_.delta(srcAddrs[input_idx])) {
                case VVal::ZERO:
                    continue;

                case VVal::ONE:
                    selector += 1 << input_idx;
                    continue;

                case VVal::X: // Any input having an X value will immediately bin the output.
                case VVal::Z: // Any inconnected input will similarly immediately bin the output.
                default:
                    return VVal::X;
            }
        }

        int maskWord = 0;

        while (selector > 64) { // FIXME: Magic numbers. Can we get the bitwidth from capnproto?
            maskWord++;
            selector -= 64; // Is this premature optimisation? Would integer division & modulo be quicker? Hmm...
        }

        unsigned int maskWordValue = op.getMask()[maskWord];

        return VVal((maskWordValue & (1 << selector)) > 0);
    }

    double Simulator::clockRate() const {
        return lastCycles_ / lastTime_;
    }

    double Simulator::runTime() const {
        return lastTime_;
    }
}
