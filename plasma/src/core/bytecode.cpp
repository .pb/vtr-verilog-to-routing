#include "bytecode.h"

#include "garble.capnp.h"
#include <capnp/message.h>

#include <cstdio>
#include <cstring>
#include <sstream>
#include <stdexcept>

#include "memory.h"

#define PLASMA_READ_UNCHECKED 1

using namespace garble;

namespace plasma {
    Bytecode::Bytecode(const std::string& fileName):
        fp_(Bytecode::loadFp(fileName)),
        bcreader_(fileno(fp_)),
        rootChecked_(bcreader_.getRoot<Executable>()),
        bcBuffer_(),
        root_() {
#if PLASMA_READ_UNCHECKED
        // Let's build an unchecked reader to ensure we're not paying for bounds checking
        bcBuffer_ = ::kj::heapArray<::capnp::word>(rootChecked_.totalSize().wordCount + 1);
        memset(bcBuffer_.begin(), 0, bcBuffer_.asBytes().size());
        ::capnp::copyToUnchecked(rootChecked_, bcBuffer_);
        root_ = ::capnp::readMessageUnchecked<::garble::Executable>(bcBuffer_.begin());
#else
        root_ = rootChecked_;
#endif
    }

    Bytecode::~Bytecode() {
        if (fp_ != nullptr)
            fclose(fp_);
    }

    FILE* Bytecode::loadFp(const std::string& fileName) {
        FILE* fp_ = fopen(fileName.c_str(), "r");

        if (fp_ == nullptr) {
            std::stringstream err;
            err << "Couldn't open bytecode file: " << std::strerror(errno) << std::endl;
            throw std::runtime_error(err.str());
        }

        return fp_;
    }

    Executable::Reader Bytecode::operator()() const {
        return root_;
    }

    InstructionSegment::Reader Bytecode::getSegment(const std::string& name){
        for (InstructionSegment::Reader r : root_.getISegs()) {
            if (r.getName() == name)
                return r;
        }
    }
}
