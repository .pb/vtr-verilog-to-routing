#include "input_manager.h"
#include "libvcd_reader.h"
#include "libvcd_lexer.h"
#include "vcd.tab.hh"

#include <cstdio>

namespace plasma {
    InputManager::InputManager(libvcd::vcd& in, MemorySpace& mem):
        dump_(in), reader_(dump_), mem_(mem), has_stim_(false) {
        populateNetLookup();
    }

    void InputManager::populateNetLookup() {
        net_lookup_.clear();

        for (const auto& netInfo : dump_.getNets()) {
            try {
                net_lookup_.emplace_back(netInfo.first, mem_.lookupAddr(netInfo.second));
            } catch (...) {
                // Whatever - there was a stimulus net we don't know about. Sad.
            }
        }
    }

    void InputManager::readInputs(int t) {
        if (!has_stim_)
            return;

        reader_.seek(t);

        for (const auto& net : net_lookup_)
            mem_.delta(net.second) = reader_.scalar(net.first);
    }

    void InputManager::rewind() {
        reader_.rewind();
    }

    void InputManager::configureInputFile(const std::string& inputFile,
                                            int* num_cycles) {
        FILE* f = fopen(inputFile.c_str(), "r");

        if (f == nullptr)
            throw std::runtime_error("Could not open input stimulus file");


        libvcd::vcd_tokwriter writer(dump_);
        libvcd::lexer lex;
        lex.setSourceFile(f);

        libvcd::parser p(lex, writer);

        if (p.parse())
            throw std::runtime_error("Failed to parse input stimulus file.");

        rewind();

        has_stim_ = true;

        populateNetLookup();

        if (num_cycles != nullptr) {
            *num_cycles = reader_.endsAt();
        }
    }
}
