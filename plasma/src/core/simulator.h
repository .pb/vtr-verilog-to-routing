#ifndef PLASMA_SIMULATOR_H
#define PLASMA_SIMULATOR_H

#include "bytecode.h"
#include "memory_space.h"
#include "input_manager.h"
#include "output_manager.h"

namespace plasma {
    typedef std::vector<std::pair<int, std::string>> SegmentSchedule;
    class Simulator {
        public:
            Simulator(Bytecode& bc,
                        MemorySpace& mem,
                        InputManager& in,
                        OutputManager& out);

            int run(const SegmentSchedule& schedule, int num_cycles, int t = 0);
            double clockRate() const;
            double runTime() const;

        protected:
            libvcd::VVal exec(::garble::InstrMovOmega::Reader op);
            libvcd::VVal exec(::garble::InstrLut::Reader op);

            Bytecode& bc_;
            MemorySpace& mem_;
            InputManager& in_;
            OutputManager& out_;

            double lastTime_;
            int lastCycles_;
    };
}

#endif
