#ifndef PLASMA_OUTPUT_MANAGER_H
#define PLASMA_OUTPUT_MANAGER_H

#include "memory_space.h"

#include "libvcd_vcd.h"
#include "libvcd_writer.h"
#include "net_mapper.h"

#include <fstream>

namespace plasma {
    class OutputManager : public NetMapper {
    public:
        OutputManager(libvcd::vcd& dump, MemorySpace& mem);

        void writeOutputs(int t);
        void addTap(const std::string& tapDesc);
        void addTapFile(const std::string& tapDesc);
        void configureDumpFile(const std::string& dumpFile);
        void configureMetadata(const std::string& program,
                               const std::string& segment,
                               const std::string& stimulus);
        void expect(int n);
        void writeDump();
        void flushMem(int t);

    protected:
        libvcd::vcd& dump_;
        libvcd::vcd_writer writer_;
        MemorySpace& mem_;
        std::ofstream dumpFile_;
    };
}

#endif
