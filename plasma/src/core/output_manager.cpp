#include "output_manager.h"

#include <string>
#include <sstream>
#include <fstream>
#include <stdexcept>

#define PLASMA_DIFF_DELTAS 1

namespace plasma {
    OutputManager::OutputManager(libvcd::vcd& dump, MemorySpace& mem):
        dump_(dump), writer_(dump_), mem_(mem) {
        writer_.setVersion("PLASMA");
        writer_.setTimescale(1, libvcd::time_unit::NANO);
    }

    void OutputManager::configureMetadata(const std::string& program,
                                            const std::string& segment,
                                            const std::string& stimulus) {
        std::stringstream meta;

        meta << "Simulation output from PLASMA" << std::endl
                << "Bytecode source: " << program << std::endl
                << "Segment: " << segment << std::endl;

        if (stimulus.size())
            meta << "Stimulus: " << stimulus << std::endl;

        writer_.setFileComment(meta.str());
    }

    void OutputManager::flushMem(int t) {
        writer_.time(t);
        writer_.setCurrentType(libvcd::dump_type::VARS);

        for (const auto& net : net_lookup_)
            writer_.scalar(net.first, mem_.omega(net.second, false));
    }

    void OutputManager::writeOutputs(int t) {
#if PLASMA_DIFF_DELTAS
        bool foundData = false;

        for (const auto& net : net_lookup_) {
            if (!(mem_.delta(net.second) == mem_.omega(net.second))) {
                writer_.time(t);
                foundData = true;
                break;
            }
        }
        // No difference between address spaces. We're done here.
        if (!foundData)
            return;
#endif

        for (const auto& net : net_lookup_) {
            writer_.scalar(net.first, mem_.delta(net.second));
        }
    }

    void OutputManager::addTap(const std::string& tapDesc) {
        MemorySpace::DAddr tapAddr;
        std::string dumpName;

        if (tapDesc.substr(0, 2) != "0x") {
            // Look up net name in bitstream
            try {
                tapAddr = mem_.lookupAddr(tapDesc);
                dumpName = tapDesc;
            } catch (...) {
                // I don't care what went wrong, we're not tapping this net.
                std::cout << "W failed to register tap having specifier '"
                            << tapDesc << "'." << std::endl;
                return;
            }
        } else if (tapDesc.size() > 2){
            // Net address is specified in string
            std::stringstream ss, sanitisedName;
            ss << tapDesc.substr(2);
            ss >> tapAddr;

            /* FIXME: Add error checking to ensure that net address
             8 is actually within the memory space */

            bool isNamedInBitstream;
            std::string netName = mem_.lookupName(tapAddr, &isNamedInBitstream);

            if (isNamedInBitstream)
                sanitisedName << netName;
            else
                sanitisedName << "__tap_" << tapAddr;

            dumpName = sanitisedName.str();
        } else {
            return;
        }

        std::cout << "i Tapped net " << dumpName << std::endl;

        writer_.enterScope(libvcd::scope_type::MODULE, "__PLASMA__");
        libvcd::net::id_t netId = writer_.declScalar(libvcd::var_type::WIRE, dumpName);
        writer_.upscope();

        net_lookup_.emplace_back(netId, tapAddr);
    }

    void OutputManager::addTapFile(const std::string& tapFileName) {
        std::ifstream tapFile;
        tapFile.open(tapFileName);

        if (!tapFile)
            throw std::runtime_error("Failed to open tap description file.");

        std::string tapDesc;

        while (tapFile >> tapDesc)
            addTap(tapDesc);
    }

    void OutputManager::configureDumpFile(const std::string& dumpFile) {
        dumpFile_.open(dumpFile);

        if (!dumpFile_)
            throw std::runtime_error("Failed to open dump file for writing");
    }

    void OutputManager::writeDump() {
        dumpFile_ << dump_;
    }

    void OutputManager::expect(int n) {
        dump_.data().scalars().reserve(n);
    }
}
