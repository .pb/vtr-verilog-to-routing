#ifndef PLASMA_INPUT_MANAGER_H
#define PLASMA_INPUT_MANAGER_H

#include "libvcd_vcd.h"
#include "libvcd_reader.h"
#include "memory_space.h"
#include "net_mapper.h"

namespace plasma {
    class InputManager : public NetMapper {
    public:
        InputManager(libvcd::vcd& in, MemorySpace& m);

        void configureInputFile(const std::string& inputFile,
                                int* num_cycles);
        void readInputs(int t);
        void rewind();

    protected:
        void populateNetLookup();

        libvcd::vcd& dump_;
        libvcd::vcd_reader reader_;
        MemorySpace& mem_;
        bool has_stim_;
    };
}

#endif
