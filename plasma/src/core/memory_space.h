#ifndef PLASMA_MEMORY_SPACE_H
#define PLASMA_MEMORY_SPACE_H

#include "bytecode.h"
#include "libvcd_vval.h"

#include <vector>
#include <map>
#include <stdexcept>

namespace plasma {
    class MemorySpace {
    public:
        typedef std::vector<libvcd::VVal> DMem;
        typedef DMem::size_type DAddr;

        MemorySpace(Bytecode& bc);

        libvcd::VVal& omega(DAddr idx, bool barrier = true);
        libvcd::VVal& delta(DAddr idx);
        DMem& omega();
        DMem& delta();
        DAddr lookupAddr(const std::string& netName) const;
        const std::string lookupName(DAddr addr, bool* success = nullptr) const;
        void swap();

    protected:
        Bytecode& bc_;
        garble::DataSpaceSpec::Reader dspec_;
        DMem memA_, memB_;
        DMem *omega_, *delta_;
        DAddr omega_barrier_;
        std::map<std::string, DAddr> net_lookup_;
        std::map<DAddr, std::string> net_name_lookup_;

        void initialiseSpace();
    };

    class OmegaBarrierViolation : public std::domain_error {
    public:
        OmegaBarrierViolation(MemorySpace::DAddr operand,
                                MemorySpace::DAddr barrier);
        virtual const char* what();
    protected:
        MemorySpace::DAddr op_;
        MemorySpace::DAddr barrier_;
        char whatBuffer_[50]; // FIXME: Magic number needs define
    };
}

#endif
