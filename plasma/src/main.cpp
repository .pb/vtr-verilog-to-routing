/**
 * PLASMA, the PLA Simuluation MAchine.
 *
 * @author Peter H. A. Bridgman <phb12@ic.ac.uk>
 */

#include "libvcd_vcd.h"

#include "bytecode.h"
#include "memory_space.h"
#include "option_manager.h"
#include "simulator.h"
#include "input_manager.h"
#include "output_manager.h"

using namespace plasma;

int main (int argc, char* argv[]) {
    std::cout << "i Welcome to PLASMA, the PLA Simulation MAchine." << std::endl;
    OptionManager opts(argc, argv);

    Bytecode bc(opts.bcFile());
    MemorySpace mem(bc);

    libvcd::vcd input, output;

    InputManager in(input, mem);
    OutputManager out(output, mem);

    opts.configure(in);
    opts.configure(out);

    Simulator sim(bc, mem, in, out);
    int cycles = sim.run(opts.segmentSchedule(), opts.cycles());

    out.writeDump();

    std::cout << std::endl << "i Simulation terminated after "
                << cycles << " cycles." << std::endl
                << "t Runtime: " << sim.runTime() << " s" << std::endl
                << "t Effective clock: " << sim.clockRate() / 1000 << " KHz"
                << std::endl;

    return 0;
}
