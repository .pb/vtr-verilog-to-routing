#include "scopetimer.h"
namespace plasma {
    scopetimer::scopetimer(double& dest):
        destination_(dest) {
        start_at = std::chrono::steady_clock::now();
    }

    scopetimer::~scopetimer() {
        auto end_at = std::chrono::steady_clock::now();
        std::chrono::duration<double> time = end_at - start_at;
        destination_ = time.count();
    }
}
