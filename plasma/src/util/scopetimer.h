#ifndef H_PLASMA_SCOPETIMER_H
#define H_PLASMA_SCOPETIMER_H

#include <chrono>

namespace plasma {
    class scopetimer {
    public:
        scopetimer(double& destination);
        scopetimer(scopetimer&) = delete;
        scopetimer& operator=(scopetimer&) = delete;
        ~scopetimer();

    protected:
        double& destination_;
        std::chrono::time_point<std::chrono::steady_clock> start_at;
    };
}

#endif
