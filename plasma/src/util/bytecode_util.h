#ifndef PLASMA_BYTECODE_UTIL_H
#define PLASMA_BYTECODE_UTIL_H

#include "libvcd_vval.h"
#include "garble.capnp.h"

namespace plasma {
    libvcd::VVal netstate_to_vval(::garble::NetState::Reader net);
}

#endif
