#include "option_manager.h"
#include "argparse.hpp"

namespace plasma {
    OptionManager::OptionManager(int argc, char* argv[]) {
        auto parser = argparse::ArgumentParser(argv[0],
                                                "PLASMA: The PLA Simulation MAchine");

        parser.version("v 0.0.1");
        parser.add_argument(args_.bytecodeFile, "bytecode")
                .help("Input bytecode file");

        parser.add_argument(args_.bytecodeSegment, "segment")
                .help("Bytecode segment to execute");

        parser.add_argument(args_.num_cycles, "--cycles")
                .help("Number of cycles to run");

        parser.add_argument(args_.dumpFile, "--dumpfile")
                .help("Name of output dump file");

        parser.add_argument(args_.tapNets, "--tap")
                .help("Named net or hexadecimal net address to be tapped")
                .nargs('*');

        parser.add_argument(args_.tapFile, "--tapfile")
                .help("Tap file specifying nets to be tapped");

        parser.add_argument(args_.stimulusFile, "--stimfile")
                .help("Stimulus VCD file");

        parser.add_argument(args_.patchSegs, "--patch-seg")
                .help("Segment to be patched into execution flow, in the form name:time")
                .nargs('*');

        parser.add_argument(args_.expectDeltas, "--expect")
                .help("Hint to the output driver how many output deltas it should expect to need to store");

        parser.add_argument(args_.show_version, "--version")
                .help("Show version")
                .action(argparse::Action::VERSION);

        parser.parse_args(argc, argv);
    }

    void OptionManager::configure(InputManager& in) {
        if (args_.stimulusFile.provenance() != argparse::Provenance::UNSPECIFIED)
            in.configureInputFile(args_.stimulusFile, &input_cycles_);
    }

    void OptionManager::configure(OutputManager& out) {
        if (args_.tapFile.provenance() != argparse::Provenance::UNSPECIFIED)
            out.addTapFile(args_.tapFile);

        for (const std::string& netName : args_.tapNets.value())
            out.addTap(netName);

        if (args_.dumpFile.provenance() == argparse::Provenance::UNSPECIFIED) {
            std::stringstream ss;
            ss << "pldump_" << args_.bytecodeFile << "_" << args_.bytecodeSegment
                << ".vcd";
            out.configureDumpFile(ss.str());
        } else {
            out.configureDumpFile(args_.dumpFile);
        }

        out.configureMetadata(args_.bytecodeFile, args_.bytecodeSegment, args_.stimulusFile);

        if (args_.expectDeltas.provenance() != argparse::Provenance::UNSPECIFIED) {
            out.expect(args_.expectDeltas);
        }
    }

    const std::string OptionManager::bcFile() const {
        return args_.bytecodeFile;
    }

    const SegmentSchedule OptionManager::segmentSchedule()  const {
        SegmentSchedule sched;

        sched.emplace_back(std::make_pair(0, args_.bytecodeSegment));

        for (const std::string& patch : args_.patchSegs.value()) {
            int sep = patch.find_first_of(':');

            if (sep == std::string::npos) {
                std::cout << "W ignored invalid patch (no time spec)" << std::endl;
                continue;
            }

            try {
                std::string seg = patch.substr(0, sep);
                int t = stoi(patch.substr(sep + 1));

                if (!(patch.size() && t))
                    throw;

                sched.emplace_back(std::make_pair(t, seg));
            } catch (...) {
                std::cout << "W ignored invalid patch (couldn't parse)" << std::endl;
            }
        }

        return sched;
    }

    int OptionManager::cycles() const {
        if (args_.num_cycles.provenance() == argparse::Provenance::UNSPECIFIED) {
            if (!input_cycles_) {
                std::cout << "\t\"The eternal silence of these infinite spaces "
                            << "frightens me.\"" << std::endl << std::endl
                            << "You should specify when we should stop simulating, "
                            << "otherwise we'll be here an awfully long time."
                            << std::endl << "Try specifying --cycles, or providing "
                            << "a --stimulusfile." << std::endl;
                throw std::invalid_argument("No simulation length specified.");
            }

            return input_cycles_;
        }

        return args_.num_cycles;
    }
}
