#ifndef PLASMA_OPTION_MANAGER_H
#define PLASMA_OPTION_MANAGER_H

#include "input_manager.h"
#include "output_manager.h"
#include "simulator.h"

#include "argparse.hpp"

#include <string>

namespace plasma {
    using argparse::ArgValue;

    struct Args {
        ArgValue<std::string> bytecodeFile;
        ArgValue<std::string> bytecodeSegment;
        ArgValue<int> num_cycles;
        ArgValue<bool> show_version;
        ArgValue<std::vector<std::string>> tapNets;
        ArgValue<std::string> tapFile;
        ArgValue<std::string> stimulusFile;
        ArgValue<std::string> dumpFile;
        ArgValue<std::vector<std::string>> patchSegs;
        ArgValue<int> expectDeltas;
    };

    class OptionManager {
    public:
        OptionManager(int argc, char* argv[]);

        void configure(InputManager& in);
        void configure(OutputManager& out);

        const std::string bcFile() const;
        const SegmentSchedule segmentSchedule() const;
        int cycles() const;

    protected:
        Args args_;
        int input_cycles_ = 0;
    };
}

#endif
