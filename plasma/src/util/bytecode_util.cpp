#include "bytecode_util.h"

namespace plasma {
    libvcd::VVal netstate_to_vval(::garble::NetState::Reader net) {
        switch (net.which()) {
            case ::garble::NetState::ZERO:
                return libvcd::VVal::ZERO;

            case ::garble::NetState::ONE:
                return libvcd::VVal::ONE;

            case ::garble::NetState::OPEN:
                return libvcd::VVal::Z;

            default:
            case ::garble::NetState::X:
                return libvcd::VVal::X;
        }
    }
}
