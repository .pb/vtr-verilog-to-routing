cmake_minimum_required(VERSION 2.8.12)

project("fabric")

include(CheckCXXSymbolExists)

#Collect the source files
file(GLOB_RECURSE EXEC_SOURCES src/main.cpp)
file(GLOB_RECURSE LIB_SOURCES src/*/*.cpp)
file(GLOB_RECURSE LIB_HEADERS src/*/*.h)
files_to_dirs(LIB_HEADERS LIB_INCLUDE_DIRS)

#Create the library
add_library(libfabric STATIC
             ${LIB_HEADERS}
             ${LIB_SOURCES})
target_include_directories(libfabric PUBLIC ${LIB_INCLUDE_DIRS})
set_target_properties(libfabric PROPERTIES PREFIX "") #Avoid extra 'lib' prefix

find_library(
    LIBVPR,
    NAMES libvpr
    HINTS ${CMAKE_SOURCE_DIR}/vpr
    NO_DEFAULT_PATH
)

#Specify link-time dependancies
target_link_libraries(libfabric
                        libvpr
                        libvtrutil
                        libarchfpga
                        libargparse
                        libgarble
                        libpugixml)

#Create the executable
add_executable(fabric ${EXEC_SOURCES})
target_link_libraries(fabric
                         -Wl,--whole-archive
                        libfabric
                        -Wl,--no-whole-archive)

install(TARGETS fabric libfabric DESTINATION bin)
