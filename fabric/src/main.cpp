/**
 * FABrIC, the FpgA BItstream Compiler.
 *
 * @author Peter H. A. Bridgman <phb12@ic.ac.uk>
 */

#include "opts.h"
#include "exit.h"
#include "exceptions.h"
#include "fabric.h"
#include "stages.h"
#include "casts.h"
#include "timer.h"


using namespace fabric;

int main(int argc, char* argv[])
try {
    TalkativeExecutionTimer t("FABrIC invocation");

    std::cout << "i Welcome to FABrIC, the FpgA BItstream Compiler." << std::endl;

    OptionParser options(argc, argv);
    FabricInstance f;
    options.configure(f);

    f.run(Stages::LoadBitstream::id);
    f.run(Stages::AnalyseBitstream::id);
    f.run(Stages::GenerateComputationGraph::id);
    f.run(Stages::CompileBytecode::id);
    f.run(Stages::WriteBytecode::id);

    std::cout << std::endl << "i Execution complete." << std::endl;

    return to_underlying(ExitStatus::SUCCESS);
} catch (UserException& e) {
    handle_failure(e);
    return to_underlying(ExitStatus::USER_FAIL);
} catch (fabric::InputException& e) {
    handle_failure(e);
    return to_underlying(ExitStatus::INPUT_FAIL);
} catch (fabric::GeneralException& e) {
    handle_failure(e);
    return to_underlying(ExitStatus::PROG_FAIL);
} catch (std::exception& e) {
    handle_failure(e);
    return to_underlying(ExitStatus::PROG_FAIL);
}
