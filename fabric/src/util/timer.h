#ifndef FABRIC_TIMER_H
#define FABRIC_TIMER_H

#include <functional>
#include <chrono>

namespace fabric {

    class ExecutionTimer {
    public:
        ExecutionTimer() {
            start_at = std::chrono::steady_clock::now();
        }

        virtual ~ExecutionTimer() {}

    protected:
        std::chrono::time_point<std::chrono::steady_clock> start_at;

        std::chrono::duration<double> endTime() {
            auto end_at = std::chrono::steady_clock::now();
            std::chrono::duration<double> time = end_at - start_at;
            return time;
        }
    };

    class CallbackTimer : public ExecutionTimer {
    public:
        CallbackTimer(std::function<void(std::chrono::duration<double>)> complete_callback):
        ExecutionTimer(), on_complete(complete_callback) {
        }

        virtual ~CallbackTimer() {
            on_complete(endTime());
        }

    protected:
        std::function<void(std::chrono::duration<double>)> on_complete;
    };

    class TalkativeExecutionTimer : public ExecutionTimer {
    public:
        TalkativeExecutionTimer(std::string description_text):
            description(description_text) {
        }

        void printEndTime() {
            std::cout << "i "
                        << description
                        << " took: "
                        << this->endTime().count()
                        << " s."
                        << std::endl;
        }

        ~TalkativeExecutionTimer() {
            printEndTime();
        }

    protected:
        std::string description;
    };
}

#endif
