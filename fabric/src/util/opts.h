#ifndef FABRIC_OPTS_H
#define FABRIC_OPTS_H

#include "fabric.h"
#include "argparse.hpp"

namespace fabric {
    using argparse::ArgValue;

    struct Args {
        ArgValue<std::string> bitstreamFile;
        ArgValue<bool> show_version;
    };

    class OptionParser {
        public:
            OptionParser(int argc, char* argv[]);

            void configure(FabricInstance& instance);

        protected:
            Args args;
    };
}

#endif
