#ifndef FABRIC_HASH_H
#define FABRIC_HASH_H

constexpr unsigned int hash_djb2(const char* input) {
    return *input ? static_cast<unsigned int>(*input) +
                        33 * hash_djb2(input + 1)
                    : 5381;
}

constexpr unsigned int operator "" _hash(const char* input, size_t len) {
    return hash_djb2(input);
}

#endif
