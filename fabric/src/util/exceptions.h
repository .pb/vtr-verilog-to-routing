#ifndef FABRIC_EXCEPTIONS_H
#define FABRIC_EXCEPTIONS_H

#include <exception>
#include <stdexcept>

namespace fabric {
    class GeneralException : public std::runtime_error {
    public:
        GeneralException(const std::string description):
            std::runtime_error(description) {
        }
    };

    class InputException : public GeneralException {
    public:
        InputException(const std::string description):
            GeneralException(description) {
        }
    };

    class UserException : public GeneralException {
    };

    void handle_failure(GeneralException& e);
    void handle_failure(InputException& e);
    void handle_failure(UserException& e);

    void handle_failure(std::exception& e);
}

#endif
