#include "opts.h"

#include "argparse.hpp"

#include <string>

namespace fabric {
    OptionParser::OptionParser(int argc, char* argv[]) {
        auto parser = argparse::ArgumentParser(argv[0],
                                                "FABrIC: the FpgA BItstream Compiler");

        parser.version("v 0.0.1");
        parser.add_argument(args.bitstreamFile, "bitstream")
            .help("Input bitstream file");

        parser.add_argument(args.show_version, "--version")
            .help("Show version")
            .action(argparse::Action::VERSION);

        parser.parse_args(argc, argv);
    }

    void OptionParser::configure(FabricInstance& instance) {
        instance.setBitstreamFilename(args.bitstreamFile);
    }
}
