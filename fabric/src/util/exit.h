#include <cstdlib>

namespace fabric {
    enum class ExitStatus {
        SUCCESS = EXIT_SUCCESS,
        PROG_FAIL = EXIT_FAILURE,
        USER_FAIL,
        INPUT_FAIL
    };
}
