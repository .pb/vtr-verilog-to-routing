#include "exceptions.h"

#include <iostream>

namespace fabric {
    void handle_failure(GeneralException& e) {
        std::cout << "Eg " << e.what() << std::endl;
    }

    void handle_failure(InputException& e) {
        std::cout << "Ei " << e.what() << std::endl;
    }

    void handle_failure(UserException& e) {
        std::cout << "Eu " << e.what() << std::endl;
    }

    void handle_failure(std::exception& e) {
        std::cout << "E? " << e.what() << std::endl;
    }
}
