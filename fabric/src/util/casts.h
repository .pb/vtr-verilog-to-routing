#ifndef FABRIC_CASTS_H
#define FABRIC_CASTS_H

#include <type_traits>

namespace fabric {
    template <typename T>
    constexpr auto to_underlying(T e) {
        return static_cast<std::underlying_type_t<T>>(e);
    }
}

#endif
