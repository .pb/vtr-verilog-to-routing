#include "generate_computation_graph.h"

namespace fabric {
    namespace Stages {
        void GenerateComputationGraph::run(fabric::FabricInstance *fab) {
            TracedDevice& td = fab->getTracedDevice();
            ComputationGraph& g = fab->getComputationGraph();

            std::vector<t_rr_idx> outputs = td.outputs();

            for (t_rr_idx output : outputs) {
                source_set sources = td.source_of_rr(output);
                t_cg_idx cgnode = g.addOutputNode(output, td.io(output));
                std::cout << "-- Out: cg:" << cgnode << std::endl;

                processRRSources(td, g, cgnode, 1);
            }
        }

        void GenerateComputationGraph::processRRSources(TracedDevice& td,
                                                        ComputationGraph& g,
                                                        t_cg_idx cgnode,
                                                        int sourceDepth) {
            const source_set ss = td.source_of_rr(g.node(cgnode).rr_idx);
            std::cout << "-- Proc RR:";
            processSourceSet(ss, td, g, cgnode, sourceDepth);
        }

        void GenerateComputationGraph::processPrimSources(TracedDevice& td,
                                                            ComputationGraph& g,
                                                            t_cg_idx cgnode,
                                                            int sourceDepth) {
            const source_set ss = td.source_of_primitive(g.node(cgnode).prim_idx);
            std::cout << "-- Proc prim:";
            processSourceSet(ss, td, g, cgnode, sourceDepth);
        }

        void GenerateComputationGraph::processSourceSet(const source_set& ss,
                                                            TracedDevice& td,
                                                            ComputationGraph& g,
                                                            t_cg_idx cgnode,
                                                            int sourceDepth) {
            std::cout << cgnode << ": " << ss << std::endl;

            for (t_rr_idx input_rr : ss.input_sources) {
                t_cg_idx input_cg = g.addInputNode(input_rr, td.io(input_rr), sourceDepth);
                g.addEdge(input_cg, cgnode);
            }

            for (int prim_id : ss.primitives) {
                const primitive& prim = td.getPrimitive(prim_id);
                t_cg_idx prim_cg = g.addPrimNode(prim_id,
                                                    prim.pb,
                                                    sourceDepth,
                                                    prim.source_pins);
                g.addEdge(prim_cg, cgnode);

                processPrimSources(td, g, prim_cg, sourceDepth + 1);
            }
        }
    }
}

FABRIC_REGISTER_STAGE(GenerateComputationGraph)
