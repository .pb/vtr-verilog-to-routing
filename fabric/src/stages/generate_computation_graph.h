#ifndef FABRIC_STAGE_GENERATE_COMPUTATION_GRAPH_H
#define FABRIC_STAGE_GENERATE_COMPUTATION_GRAPH_H

#include "stage.h"
#include "fabric.h"

#include <iostream>

namespace fabric {
    namespace Stages {
        class GenerateComputationGraph : public StageBase {
        FABRIC_INIT_STAGE
        public:
            GenerateComputationGraph() {}

            void run(fabric::FabricInstance *fab);

            void processRRSources(TracedDevice& td,
                                    ComputationGraph& g,
                                    t_cg_idx cgnode,
                                    int sourceDepth);
            void processPrimSources(TracedDevice& td,
                                     ComputationGraph& g,
                                     t_cg_idx cgnode,
                                     int sourceDepth);
            void processSourceSet(const source_set& ss,
                                    TracedDevice& td,
                                     ComputationGraph& g,
                                     t_cg_idx cgnode,
                                     int sourceDepth);
        };
    }
}

#endif
