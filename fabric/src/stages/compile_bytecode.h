#ifndef FABRIC_STAGE_COMPILE_BYTECODE_H
#define FABRIC_STAGE_COMPILE_BYTECODE_H

#include "stage.h"
#include "fabric.h"

#include "computation_graph.h"
#include "bitstream_blocks.h"

#include <iostream>

namespace fabric {
    namespace Stages {
        class CompileBytecode : public StageBase {
        FABRIC_INIT_STAGE
        public:
            CompileBytecode() {}

            void run(fabric::FabricInstance *fab);
            void printInputs(ComputationGraph& cg, ExecutionGraph& eg);
            void printInstrList(const InstructionList& l);
            void printMemoryMap(const MemoryMap& m);
        protected:
            void buildInstructionList(ComputationGraph& g,
                                      ExecutionGraph& e,
                                       InstructionList& l,
                                       const MemoryMap& memoryMap,
                                       const PBPrimitiveMap& blockMap);
            void buildMemoryMap(ComputationGraph& g,
                                  ExecutionGraph& e,
                                   MemoryMap& memoryMap);
       };
    }
}

#endif
