#ifndef FABRIC_STAGE_ANALYSE_BITSTREAM_H
#define FABRIC_STAGE_ANALYSE_BITSTREAM_H

#include "fabric.h"
#include "stage.h"
#include "bitstream_blocks.h"
#include "rr_source_lookup.h"
#include "swizzle_data.h"
#include "traced_device.h"

#include <unordered_set>
#include <list>
#include <vector>

#include "vpr_types.h"

#include "physical_types.h"

namespace fabric {
    namespace Stages {
        class AnalyseBitstream : public StageBase {
        FABRIC_INIT_STAGE
        public:
            AnalyseBitstream() {}

            void run(fabric::FabricInstance *fab);

            typedef std::map<int, t_pb*> BlockPBPinLookup;
            typedef std::map<ClusterBlockId, BlockPBPinLookup> PBPinLookupSet;

            const t_rr_idx INVALID_IDX = -1;

        protected:
            void configureTLBs(BitstreamBlocks& blocks,
                                PBPinLookupSet& pb_pin_lookup_set);
            void configureTLB(const BitstreamTLB& tlb,
                                const t_type_descriptor* type_desc,
                                BlockPBPinLookup& pb_pin_lookup,
                                BitstreamBlocks& blocks);
            void configureTLBPorts(const BitstreamTLB& tlb, t_pb *pb);
            t_pb** configureChildPBs(t_pb* pb,
                                        const ChildBlockLookup& children,
                                        BitstreamBlocks& blocks,
                                        BlockPBPinLookup& pb_pin_lookup,
                                        t_pb* tlb);
            void configureInterconnect(const BitstreamBlock& block,
                                        t_pb *pb,
                                        t_pb *tlb);

            void configureChildPb(t_pb* pb,
                                    const BitstreamBlock& block,
                                    BitstreamBlocks& blocks,
                                    BlockPBPinLookup& pb_pin_lookup,
                                    t_pb* tlb);

            int findInterconnectIndex(const t_mode& mode, const std::string& iconName);
            std::vector<int> getBlockDirectInterconnect(const t_mode& mode);
            int findChildTypeIndex(const t_mode& mode, const std::string& typeName);

            void configureBlockPlacement(const BitstreamTLB& clb);

            const t_type_descriptor* findTLBtype_descriptor(
                                        BitstreamBlockTypeIndex bstm_typIdx,
                                        const BitstreamBlocks& blocks);
            t_block_loc& getBlockLoc(BitstreamToplevelBlockId id);

            void buildRoutingTraces(const BitstreamBlocks& blocks,
                                    const rr_source_lookup& sourceLookup,
                                    const PBPinLookupSet& pin_lookup_set,
                                    const std::set<t_rr_switchbit_id>& bstm_sbits,
                                    const SwizzleData& swizzle,
                                    TracedDevice& td);
            std::vector<t_rr_idx> getNodeListFromTLBList(const std::vector<const BitstreamTLB*>& tlbs,
                                                            std::vector<t_pb*>* nodePBList);
            t_rr_setList generateNodeSetList(const std::vector<t_rr_idx>& nodes);
            t_rr_setList generateNodeSetList(const t_rr_set& nodes);

            int doSwitchbitGlob(t_rr_setList& nodeSetList,
                                const rr_source_lookup& sourceLookup,
                                std::set<t_rr_switchbit_id>& sbits);
            void doNodeSetAmalgamation(t_rr_setList& nodeSetList);

            // Do these two sets intersect?
            bool setsIntersect(const t_rr_set& set1, const t_rr_set& set2);

            // Find the set of Ipins that are reached from the set of CLB Opins given
            t_rr_set traceOpinsToIpins(const std::vector<t_rr_idx>& opins,
                                        const PBPinLookupSet& pin_lookup_set,
                                        const BitstreamBlocks& blocks,
                                        const SwizzleData& swizzle,
                                        TracedDevice& td);

            void build_pb_pin_lookup(t_pb* pb,
                                        BlockPBPinLookup& lookup);

            void filterInvalidNodes(t_rr_setList& list);
            void filterInvalidNodes(t_rr_set& set);
        };
    }

    std::ostream& operator<<(std::ostream& os, const t_rr_set& nodeSet);
    std::ostream& operator<<(std::ostream& os, const t_rr_setList& list);
}

#endif
