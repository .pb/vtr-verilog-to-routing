#ifndef FABRIC_STAGE_WRITE_BYTECODE_H
#define FABRIC_STAGE_WRITE_BYTECODE_H

#include "stage.h"
#include "fabric.h"

#include "garble.capnp.h"
#include <capnp/message.h>
#include <capnp/serialize.h>

#include <iostream>

namespace fabric {
    namespace Stages {
        class WriteBytecode : public StageBase {
        FABRIC_INIT_STAGE
        public:
            WriteBytecode ():
                bc_msg(), bc_root(bc_msg.initRoot<::garble::Executable>()) {
            }

            void run(fabric::FabricInstance *fab);
            void buildBytecodeExecutable();
            void writeToFile(const std::string& fileName);
            std::string getFilename(const std::string& bitstreamFilename);
        protected:
            ::capnp::MallocMessageBuilder bc_msg;
            ::garble::Executable::Builder bc_root;
        };
    }
}

#endif
