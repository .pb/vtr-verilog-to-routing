#include "analyse_bitstream.h"

#include "string.h"
#include <iterator>

#include "globals.h"
#include "clustered_netlist_fwd.h"
#include "rr_node.h"

#include "vpr_utils.h"
#include "rr_node.h"
#include "netlist_fwd.h"
#include "clustered_netlist.h"

#include "assert.h"

namespace fabric {
    namespace Stages {

        void AnalyseBitstream::run(fabric::FabricInstance *fab) {
            BitstreamBlocks& blocks = fab->getBlocks();

            TracedDevice& td = fab->getTracedDevice();

            auto& place_ctx = g_vpr_ctx.mutable_placement();
            place_ctx.grid_blocks.clear();
            place_ctx.grid_blocks.resize({blocks.dims.x, blocks.dims.y});

            PBPinLookupSet pb_pin_lookup_set;

            configureTLBs(blocks, pb_pin_lookup_set);
            buildRoutingTraces(blocks,
                                fab->getSourceLookup(),
                                pb_pin_lookup_set,
                                fab->getBstm_sbits(),
                                fab->getSwizzle(),
                                td);
        }

        void AnalyseBitstream::configureTLBs(BitstreamBlocks& blocks,
                                                PBPinLookupSet& pb_pin_lookup_set) {
            /*
             * To fully configure the block representation:
             *  For all blocks, we should:
             *  1. Instantiate the t_pb object
             *  2. Set t_pb.name to something useful
             *  3. Configure pb_graph_head pointers
             *
             * For each top level CLB & IO block:
             *  1. Set place_ctx.block_locs[blk_id].{x, y, z}
             *      (NB this may need resizing to fit!)
             *
             *  For each CLB, we must:
             *  1. Build the t_pb_route
             *
             *  Additionally, for each CLB and child thereof, we must;
             *  1. Set t_pb.mode
             */

            for (const auto& blkType : blocks.blocks.map) {
                BitstreamBlockTypeIndex bstm_typIdx = blkType.first;
                BlockTypeSet<BitstreamTLB> tlbs = blkType.second;

                const t_type_descriptor *type_desc = findTLBtype_descriptor(bstm_typIdx, blocks);

                if (!type_desc) {
                    std::cout << "W TLB with type '" << blocks.blockTypes.lookup(bstm_typIdx)
                                << "' (bitstream idx " << bstm_typIdx << ") not found in "
                                << "architecture. All " << tlbs.size() << " top-level "
                                << "instances of this block type will be ignored."
                                << std::endl;

                    continue;
                }

                std::cout << "* Configuring " << tlbs.size() << " TLBs of type ["
                          << type_desc->name << "]" << std::endl;

                for (const auto& tlb : tlbs) {
                    auto it = pb_pin_lookup_set.emplace(ClusterBlockId(tlb.second.id), BlockPBPinLookup{});
                    configureTLB(tlb.second, type_desc, it.first->second, blocks);
                }
            }

            std::cout << "* All TLBs configured." << std::endl;
        }

        void AnalyseBitstream::configureTLB(const BitstreamTLB& tlb,
                                            const t_type_descriptor* type_desc,
                                            BlockPBPinLookup& pb_pin_lookup,
                                            BitstreamBlocks& blocks) {
            auto& cluster_ctx = g_vpr_ctx.mutable_clustering();
            ClusteredNetlist& nlist = cluster_ctx.clb_nlist;

            t_pb *pb = new t_pb;
            pb->name = strdup(tlb.name.c_str()); // FIXME: Memleak
            pb->mode = tlb.mode;

            if (tlb.mode >= type_desc->pb_graph_head->pb_type->num_modes ||
                    tlb.mode < 0) {
                std::cout << "W TLB mode index " << tlb.mode
                            << " out of range for TLB named "
                            << tlb.name << " [" << type_desc->name
                            << "]. Abandon TLB!" << std::endl;
                return;
            }

            std::cout << "-- Configuring tlb, id: " << tlb.id << ", mode: " << tlb.mode << std::endl;

            ClusterBlockId index(tlb.id);

            nlist.create_block_having_id(index,
                                            tlb.name.c_str(),
                                            pb,
                                            type_desc);

            pb->pb_graph_node = type_desc->pb_graph_head;
            pb->pb_route = new t_pb_route[pb->pb_graph_node->total_pb_pins];
            std::cout << "-- pb_gnode for this clb says we have " << pb->pb_graph_node->total_pb_pins
                        << " pins; allocating routing array." << std::endl;
            pb->child_pbs = configureChildPBs(pb, tlb.children, blocks, pb_pin_lookup, pb);

            build_pb_pin_lookup(pb, pb_pin_lookup);

            configureBlockPlacement(tlb);
            configureTLBPorts(tlb, pb);
            configureInterconnect(tlb, pb, pb);
        }

        void AnalyseBitstream::configureTLBPorts(const BitstreamTLB& tlb, t_pb *pb) {
            auto& cluster_ctx = g_vpr_ctx.mutable_clustering();
            ClusteredNetlist& nlist = cluster_ctx.clb_nlist;
            const t_pb_graph_node& pb_gnode = *pb->pb_graph_node;
            const t_pb_type& pb_type = *pb_gnode.pb_type;

            ClusterBlockId index(tlb.id);

            int num_in_ports = pb_gnode.num_input_ports;
            int num_out_ports = pb_gnode.num_output_ports;
            int num_clk_ports = pb_gnode.num_clock_ports;

            std::stringstream ss_netfake;
            ss_netfake << "??fbc??fake:" << tlb.name;
            ClusterNetId FAKE_NET = nlist.create_net(ss_netfake.str());
            bool FAKE_IS_CONST = false;

            int pin_idx = 0;
            int port_idx = 0;

            for (int i = 0; i < num_in_ports; i++) {
                ClusterPortId port = nlist.create_port(index,
                                                       pb_type.ports[port_idx].name,
                                                       pb_type.ports[port_idx].num_pins,
                                                       PortType::INPUT);

                // Why would creating a port also create its pins? Don't be silly.
                for (int j = 0; j < pb_type.ports[port_idx].num_pins; j++) {
                    const t_pb_graph_pin& pb_graph_pin = pb_gnode.input_pins[i][j];
                    assert(pb_graph_pin.pin_count_in_cluster == pin_idx);
                    nlist.create_pin(port, j, FAKE_NET, PinType::SINK, pin_idx++, FAKE_IS_CONST);
                }

                port_idx++;
            }

            for (int i = 0; i < num_out_ports; i++) {
                ClusterPortId port = nlist.create_port(index,
                                                       pb_type.ports[port_idx].name,
                                                       pb_type.ports[port_idx].num_pins,
                                                       PortType::OUTPUT);

                for (int j = 0; j < pb_type.ports[port_idx].num_pins; j++) {
                    std::stringstream ss_driver;
                    ss_driver << ss_netfake.str() << ":driver:" << j;
                    ClusterNetId FAKE_NET_DRIVER = nlist.create_net(ss_driver.str());
                    nlist.create_pin(port, j, FAKE_NET_DRIVER, PinType::DRIVER, pin_idx++, FAKE_IS_CONST);
                }

                port_idx++;
            }

            for (int i = 0; i < num_clk_ports; i++) {
                ClusterPortId port = nlist.create_port(index,
                                                       pb_type.ports[port_idx].name,
                                                       pb_type.ports[port_idx].num_pins,
                                                       PortType::CLOCK);

                for (int j = 0; j < pb_type.ports[port_idx].num_pins; j++) {
                    nlist.create_pin(port, j, FAKE_NET, PinType::SINK, pin_idx++, FAKE_IS_CONST);
                }

                port_idx++;
            }
        }

        t_pb** AnalyseBitstream::configureChildPBs(t_pb* pb,
                                                    const ChildBlockLookup& bstm_children,
                                                    BitstreamBlocks& blocks,
                                                    BlockPBPinLookup& pb_pin_lookup,
                                                    t_pb* tlb) {
            t_mode& pb_mode = pb->pb_graph_node->pb_type->modes[pb->mode];

            std::cout << "-- Configuring children: apparently we have " << pb_mode.num_pb_type_children
                        << " types of child." << std::endl;

            // Allocate children[0..num_pb_type_children - 1][0..pb_type_children[i].num_pb - 1]
            t_pb** children = new t_pb*[pb_mode.num_pb_type_children];

            int alloc_count = 0;

            for (int i = 0; i < pb_mode.num_pb_type_children; i++) {
                std::cout << "-- Child type idx " << i << "; we have "
                            << pb_mode.pb_type_children[i].num_pb << std::endl;
                const size_t num_children = pb_mode.pb_type_children[i].num_pb;
                children[i] = new t_pb[num_children];
                alloc_count += num_children;
            }

            std::cout << "-- Allocated " << alloc_count << " children." << std::endl;
            int config_count = 0;

            for (const auto& blkTypePair : bstm_children.map) {
                const char* typeName = blocks.blockTypes.lookup(blkTypePair.first);

                std::cout << "-- Configuring child type " << typeName << std::endl;

                if (!typeName) {
                    std::cout << "W No BitstreamTypeIndex in lookup for "
                                << blkTypePair.first << "!"
                                << std::endl;

                    continue;
                }

                int childPBTypeIndex = findChildTypeIndex(pb_mode, typeName);

                if (childPBTypeIndex < 0) {
                    std::cout << "W childtype [" << typeName
                                << "] appears not to be a valid child! "
                                << "Ignoring all " << blkTypePair.second.size()
                                << " children of this type."
                                << std::endl;

                    continue;
                }

                std::cout << "-- (has type index " << childPBTypeIndex << ")." << std::endl;

                for (const auto& blockPair : blkTypePair.second) {
                    const BitstreamBlock& childBlock = blockPair.second;
                    std::cout << "-- Configuring child index " << childBlock.index << std::endl;

                    if (childBlock.index < 0 ||
                            childBlock.index >= pb_mode.pb_type_children[childPBTypeIndex].num_pb) {
                        std::cout << "W Bitstream Block having index " << childBlock.index
                                    << " out of range for type! Block ignored."
                                    << std::endl;
                        continue;
                    }

                    t_pb& childPb = children[childPBTypeIndex][childBlock.index];

                    childPb.parent_pb = pb;
                    childPb.pb_graph_node = &pb->pb_graph_node->child_pb_graph_nodes[pb->mode][childPBTypeIndex][childBlock.index];

                    configureChildPb(&childPb, childBlock, blocks, pb_pin_lookup, tlb);
                    config_count++;
                }
            }

            std::cout << "-- Successfully configured " << config_count << " children of "
                        << pb->name << std::endl;

            return children;
        }

        void AnalyseBitstream::configureChildPb(t_pb* pb,
                                                const BitstreamBlock& block,
                                                BitstreamBlocks& blocks,
                                                BlockPBPinLookup& pb_pin_lookup,
                                                t_pb* tlb) {
            pb->name = strdup(block.name.c_str()); // FIXME: Memleak
            pb->mode = block.mode;
            std::cout << "-- CONFIG CHILD: " << pb->name << std::endl;
            configureInterconnect(block, pb, tlb);
            build_pb_pin_lookup(pb, pb_pin_lookup);

            if (!pb->pb_graph_node->pb_type->modes) {
                pb->child_pbs = nullptr;
                // FIXME: Taking a copy of the primitive like this is pretty horrid
                blocks.primitiveBlocks.insert(std::make_pair(pb, *block.primitive.begin()));

                std::cout << "-- Child is leaf node; configuration finished" << std::endl;
                return;
            }

            if (int num_children = pb->pb_graph_node->pb_type->modes[pb->mode].num_pb_type_children) {
                if (num_children != block.children.size()) {
                    std::cout << "W Num_children " << num_children << " != "
                                << block.children.size() << " block children count"
                                << std::endl;
                }
                pb->child_pbs = configureChildPBs(pb, block.children, blocks, pb_pin_lookup, tlb);
            } else {
                pb->child_pbs = nullptr;
                std::cout << "W: Have modes, so am not primitive, but have no children."
                            << "Seems odd... node has no processing!" << std::endl;
            }
        }

        void AnalyseBitstream::configureInterconnect(const BitstreamBlock& block,
                                                        t_pb* pb,
                                                        t_pb* tlb) {
            std::cout << "--\tConfiguring interconnect for " << pb->name << std::endl;
            if (!pb->pb_graph_node->interconnect_pins) {
                std::cout << "--\tNone found!" << std::endl;
                return; // No interconnect to configure!
            }

            const t_mode& mode = pb->pb_graph_node->pb_type->modes[pb->mode];
            const t_interconnect_pins* interconnect_pins = pb->pb_graph_node->interconnect_pins[mode.index];

            // First, handle configurable interconnect from bitstream file
            for (const BitstreamBlockInterconnect& bstm_icon : block.interconnect) {
                int icon_idx = findInterconnectIndex(mode, bstm_icon.name);

                if (icon_idx < 0) {
                    std::cout << "W Interconnect with name " << bstm_icon.name
                                << " not found; ignored." << std::endl;
                    continue;
                }

                const t_interconnect& icon = mode.interconnect[icon_idx];
                const t_interconnect_pins& icon_pins = interconnect_pins[icon_idx];

                std::cout << "--\t Interconnect " << icon.name << " (index "
                            << icon_idx << "):"<< std::endl;

                if (icon.interconnect_power->num_input_ports != bstm_icon.inputWidth) {
                    std::cout << "W Interconnect [" << bstm_icon.name << "]: "
                                << "bitstream width (" << bstm_icon.inputWidth << ") != "
                                << "architecture width (" << icon.interconnect_power->num_input_ports
                                << ")! Ignoring interconnect." << std::endl;
                    continue;
                }

                // FIXME: Check bstm_icon.type == icon->type (needs enum mapping)

                std::map<int, int> map_outId_selectedInId;

                for (const BitstreamInterconnectWay& way : bstm_icon.ways) {
                    std::cout << "--\tWay " << way.wayId << " sel "
                                << way.selectedInputIndex << std::endl;
                    map_outId_selectedInId.emplace(way.wayId, way.selectedInputIndex);
                }

                int num_outports = icon.interconnect_power->num_output_ports;

                for (int outport_idx = 0; outport_idx < num_outports; outport_idx++) {
                    const auto& selMap = map_outId_selectedInId.find(outport_idx);

                    if (selMap != map_outId_selectedInId.end()) {
                        int inport_idx = selMap->second;
                        int pin_idx = 0; // FIXME: THIS IS A HORRIBLE HACKY ASSUMPTION
                        t_pb_graph_pin *output_pin = icon_pins.output_pins[outport_idx][pin_idx];
                        t_pb_graph_pin *input_pin = icon_pins.input_pins[inport_idx][pin_idx];
                        t_pb_route& routeEdge = tlb->pb_route[output_pin->pin_count_in_cluster];

                        routeEdge.driver_pb_pin_id = input_pin->pin_count_in_cluster;
                        routeEdge.sink_pb_pin_ids.push_back(output_pin->pin_count_in_cluster);

                        std::cout << "--\tRoute " << routeEdge.driver_pb_pin_id << "->"
                                    << output_pin->pin_count_in_cluster << std::endl;

                        // FIXME: We probably ought to ensure that there are no other sinks, no?
                        // FIXME: pb_graph_pin definitely should be populated and it definitely isn't.
                        //          read_netlist has some clues on how to populate this correctly.
                    }
                }
            }

            // Now, handle DIRECT interconnects from the architecture
            for (const int icon_idx : getBlockDirectInterconnect(mode)) {
                const t_interconnect& icon = mode.interconnect[icon_idx];
                const t_interconnect_pins& icon_pins = interconnect_pins[icon_idx];

                int num_inports = icon.interconnect_power->num_input_ports;
                int num_outports = icon.interconnect_power->num_output_ports;
                int num_portpins = icon.interconnect_power->num_pins_per_port;

                for (int outport_idx = 0; outport_idx < num_outports; outport_idx++) {
                    int inport_idx = 0;

                    for (; inport_idx < num_inports; inport_idx++) {
                        for (int pin_idx = 0; pin_idx < num_portpins; pin_idx++) {
                            t_pb_graph_pin *output_pin = icon_pins.output_pins[outport_idx][pin_idx];
                            t_pb_graph_pin *input_pin = icon_pins.input_pins[inport_idx][pin_idx];
                            t_pb_route& routeEdge = tlb->pb_route[output_pin->pin_count_in_cluster];

                            routeEdge.driver_pb_pin_id = input_pin->pin_count_in_cluster;
                            routeEdge.sink_pb_pin_ids.push_back(output_pin->pin_count_in_cluster);

                            std::cout << "--\tDirect Route " << routeEdge.driver_pb_pin_id << "->"
                                        << output_pin->pin_count_in_cluster << std::endl;
                        }
                    }
                }
            }
        }

        int AnalyseBitstream::findInterconnectIndex(const t_mode& mode,
                                                    const std::string& iconName) {
            for (size_t i = 0; i < mode.num_interconnect; i++) {
                if (!iconName.compare(mode.interconnect[i].name))
                    return i;
            }

            return -1;
        }

        std::vector<int> AnalyseBitstream::getBlockDirectInterconnect(const t_mode& mode) {
            std::vector<int> icon_idx;

            for (size_t i = 0; i < mode.num_interconnect; i++) {
                if (mode.interconnect[i].type == DIRECT_INTERC)
                    icon_idx.push_back(i);
            }

            return icon_idx;
        }

        int AnalyseBitstream::findChildTypeIndex(const t_mode& mode,
                                                const std::string& typeName) {
            for (size_t i = 0; i < mode.num_pb_type_children; i++) {
                if (!typeName.compare(mode.pb_type_children[i].name))
                    return i;
            }

            return -1;
        }

        const t_type_descriptor* AnalyseBitstream::findTLBtype_descriptor(BitstreamBlockTypeIndex bstm_typIdx,
                                                                    const BitstreamBlocks& blocks) {
            auto& device_ctx = g_vpr_ctx.device();
            const char* blockTypeName = blocks.blockTypes.lookup(bstm_typIdx);

            // FIXME: Add assertion for blockTypeName != nullptr

            for (int i = 0; i < device_ctx.num_block_types; i++) {
                if (strcmp(device_ctx.block_types[i].name, blockTypeName) == 0) {
                    return &device_ctx.block_types[i];
                }
            }

            return nullptr;
        }

        void AnalyseBitstream::configureBlockPlacement(const BitstreamTLB& clb) {
            auto& place_ctx = g_vpr_ctx.mutable_placement();

            t_block_loc& loc = getBlockLoc(clb.id);
            loc.x = clb.coord.x;
            loc.y = clb.coord.y;
            loc.z = clb.coord.z;

            std::cout << "-- Configuring placement for CLB ID " << clb.id << std::endl;
            std::cout << "-- At x:" << loc.x << " y:" << loc.y << std::endl;

            t_grid_blocks& gridNode = place_ctx.grid_blocks[loc.x][loc.y];
            ClusterBlockId blockId{clb.id};

            int usage = gridNode.usage++;
            gridNode.blocks.push_back(blockId);

            std::cout << "--\tVerify placed: " << place_ctx.grid_blocks[loc.x][loc.y].usage
                        << "has blkid: " << place_ctx.grid_blocks[loc.x][loc.y].blocks[usage - 1].data()
                        << std::endl;

            place_sync_external_block_connections(blockId);
        }

        t_block_loc& AnalyseBitstream::getBlockLoc(BitstreamToplevelBlockId id) {
            auto& place_ctx = g_vpr_ctx.mutable_placement();

            // FIXME: Better to find max ID and do it once
            if (place_ctx.block_locs.size() <= id)
                place_ctx.block_locs.resize(id + 1);

            return place_ctx.block_locs[ClusterBlockId(id)];
        }

        void AnalyseBitstream::buildRoutingTraces(const BitstreamBlocks& blocks,
                                                    const rr_source_lookup& sourceLookup,
                                                    const PBPinLookupSet& pb_pin_lookup,
                                                    const std::set<t_rr_switchbit_id>& bstm_sbits,
                                                    const SwizzleData& swizzle,
                                                    TracedDevice& td) {
            /*
             * To build the traces, we perform the following algorithm:
             * 1. Identify primary SINKs- the input pins to the
             *      IO blocks performing the role of primary outputs.
             *      Place each IPIN in a routing node set by itself.
             * 2. Identify from the set of activated configurable
             *    switch-bits if there are any directly reachable from
             *    the nodes in each routing node set. If so, add the node
             *    reachable via each adjacent activated configurable
             *    switch-bit to the routing node set, and remove the
             *    switchbits from the set under consideration.
             * 3. Perform set amalgamation: if any nodes are shared between
             *    node sets, amalgamate the sets into one as they represent
             *    one logical net.
             * 4. Perform non-configurable switchbit globbing: if any
             *      non-configurable edge exists in the RR graph from
             *      any node in each routing node set, add the source
             *      node from each such edge into the routing node set.
             * 5. Perform set amalgamation again as per (3).
             * 6. Repeat from (2) until there are no further candidates
             *    for set expansion.
             * 7. Perform block traversal: for each OPIN reached at a
             *    non-IO node, traverse the CLB's local routing graph to
             *    determine the set of implicated IPINs. This is performed
             *    in the same manner as steps 2-6 but for the CLB-local
             *    routing graph.
             * 8. Add the IPINs determined during (7) to new node sets
             *    by themselves, add those node sets to the list under
             *    consideration, and repeat the algorithm from 2, until
             *    no further OPINs are reached. Then the FPGA routing
             *    is fully established.
             */

            std::vector<const BitstreamTLB*> primaryOutputs = blocks.blocks.havingDisposition(BitstreamTLBDisposition::OUTPUT);
            std::vector<t_pb*> outputNodePBs;
            std::vector<t_rr_idx> primaryOutputNodes = getNodeListFromTLBList(primaryOutputs, &outputNodePBs);
            std::cout << "* Tracing routing for "
                        << primaryOutputNodes.size() << " outputs."
                        << std::endl;

            td.markRRasOutput(primaryOutputNodes, outputNodePBs);
            t_rr_setList nodeSetList = generateNodeSetList(primaryOutputNodes);
            filterInvalidNodes(nodeSetList);
            std::set<t_rr_switchbit_id> sbits = bstm_sbits; // Take a copy for mutation

            t_rr_set tracedOpins;
            t_rr_setList newNodeSets;

            int globbedNodes = 0,
                globCycles = 0,
                tracethroughCycles = 0;

            do {
                do {
                    globbedNodes = doSwitchbitGlob(nodeSetList, sourceLookup, sbits);
                    doNodeSetAmalgamation(nodeSetList);
                    globCycles++;
                } while (globbedNodes);

                std::cout << "-- Glob set exhausted." << std::endl;

                std::vector<t_rr_idx> opinNodes = filterNodeList(filterNodeSetList(nodeSetList, OPIN), tracedOpins);
                t_rr_set implicatedIpins = traceOpinsToIpins(opinNodes,
                                                               pb_pin_lookup,
                                                               blocks,
                                                               swizzle,
                                                               td);
                std::copy(opinNodes.begin(), opinNodes.end(), std::inserter(tracedOpins, tracedOpins.end()));

                newNodeSets = generateNodeSetList(implicatedIpins);
                nodeSetList.insert(nodeSetList.end(), newNodeSets.begin(), newNodeSets.end());
                doNodeSetAmalgamation(nodeSetList);
                tracethroughCycles++;
            } while (newNodeSets.size());

            std::cout << "* Built inter-block routing traces; we performed "
                        << globCycles << " glob-amalgamate cycles and "
                        << tracethroughCycles << " tracethrough cycles."
                        << std::endl;

            std::cout << "* Final circuit contains "
                        << nodeSetList.size() << " nets."
                        << std::endl;

            std::cout << "* After trace construction, there were "
                        << sbits.size() << " unreached but configured switchbits."
                        << std::endl;

            td.nodeSets(nodeSetList);
        }

        std::vector<t_rr_idx>
            AnalyseBitstream::getNodeListFromTLBList(const std::vector<const BitstreamTLB*>& tlbs,
                                                        std::vector<t_pb*>* nodePBList) {
            std::vector<t_rr_idx> retVec{};
            auto& device_ctx = g_vpr_ctx.device();
            const auto& clustering = g_vpr_ctx.clustering();
            const ClusteredNetlist& nlist = clustering.clb_nlist;

            std::transform(tlbs.begin(), tlbs.end(), std::back_inserter(retVec),
                            [&](const BitstreamTLB* tlb) {
                int pin_idx = -1;

                ClusterBlockId blkId(tlb->id);
                t_pb *pb = nlist.block_pb(blkId);
                t_pb_graph_node *pb_gnode = pb->pb_graph_node;

                if (nodePBList)
                    nodePBList->push_back(pb);

                for (ClusterPortId portId : nlist.block_output_ports(blkId)) {
                    ClusterPinId pinId = nlist.port_pin(portId, 0); // FIXME: Assuming the ports are all 1-wide
                    pin_idx = nlist.pin_physical_index(pinId);
                }

                int block_pins = *pb_gnode->num_input_pins +
                                    *pb_gnode->num_output_pins +
                                    *pb_gnode->num_clock_pins;

                int rr_offset = (block_pins * tlb->coord.z) - 1 + pin_idx;

                auto& lookup = device_ctx.rr_node_indices[SINK][tlb->coord.x][tlb->coord.y][0];

                if (lookup[rr_offset] >= 0)
                    return lookup[rr_offset];

                std::cout << "W Failed to find RR index for output at "
                          << "(" << tlb->coord.x << "," << tlb->coord.y
                          << "," << tlb->coord.z << ")!" << std::endl;

                return -1;
            });

            return retVec;
        }

        t_rr_setList
            AnalyseBitstream::generateNodeSetList(const std::vector<t_rr_idx>& nodes) {
            t_rr_setList retVec{};

            std::transform(nodes.begin(), nodes.end(), std::back_inserter(retVec),
                            [](t_rr_idx index) {
                return t_rr_set{index};
            });

            return retVec;
        }

        /* FIXME: This is not DRY... a nice type-erasure setup with some
         * tasty templates would be much less repulsive. Ah well. */
        t_rr_setList
            AnalyseBitstream::generateNodeSetList(const t_rr_set& nodes) {
            t_rr_setList retVec{};

            std::transform(nodes.begin(), nodes.end(), std::back_inserter(retVec),
                            [](t_rr_idx index) {
                return t_rr_set{index};
            });

            return retVec;
        }

        int AnalyseBitstream::doSwitchbitGlob(t_rr_setList& nodeSetList,
                                                const rr_source_lookup& sourceLookup,
                                                std::set<t_rr_switchbit_id>& sbits) {
            auto& device_ctx = g_vpr_ctx.device();
            int globCount = 0;

            for (t_rr_set& nodeSet : nodeSetList) {
                std::vector<t_rr_idx> globbedNodes;

                for (const t_rr_idx& nodeIdx : nodeSet) {
                    std::cout << "--\tglobNode " << nodeIdx << std::endl
                                << "--\ttry[node]:sbit,...";

                    for (const auto& source : sourceLookup.getSources(nodeIdx)) {
                        t_rr_idx source_idx = source.first;

                        if (source.second) { // Is configurable?
                            t_rr_switchbit_id_map node_sbits =
                                device_ctx.rr_switchbits.get(source_idx, nodeIdx);

                            std::cout << " [" << source_idx << "]: ";

                            for (const auto& node_sbit : node_sbits) {
                                std::cout << node_sbit.first;
                                if (sbits.count(node_sbit.first)) {
                                    std::cout << " Y!, ";
                                    globbedNodes.push_back(source_idx);
                                    sbits.erase(node_sbit.first);
                                } else
                                    std::cout << "n,";
                            }


                            if (!node_sbits.size()) {
                                std::cout << "W Found no device switchbits for configurable edge "
                                            << source_idx << " -> "
                                            << nodeIdx << std::endl;
                            }
                        } else { // Unconfigurable source
                            globbedNodes.push_back(source_idx);
                        }
                    }

                    std::cout << std::endl;
                }

                if (globbedNodes.size()) {
                    std::cout << "--\t" << nodeSet << std::endl;
                }

                nodeSet.insert(globbedNodes.begin(), globbedNodes.end());
                std::cout << "--\tGlobbed " << globbedNodes.size();

                if (globbedNodes.size()) {
                    std::cout << " => " << nodeSet;
                }

                std::cout << std::endl;

                globCount += globbedNodes.size();
            }

            std::cout << "--\t" << nodeSetList;

            return globCount;
        }

        void AnalyseBitstream::doNodeSetAmalgamation(t_rr_setList& nodeSetList) {
            for (auto it = nodeSetList.begin(); it != nodeSetList.end(); it++) {
                for (auto cmpIt = std::next(it); cmpIt != nodeSetList.end(); cmpIt++) {
                    if (setsIntersect(*it, *cmpIt)) {
                        // Two node sets share nodes; they're really one net. Let's merge!
                        cmpIt->insert(it->begin(), it->end());

                        it->clear();
                        /* We're also now done with this iteration of the outer loop; the set
                         * has been amalgamated into a later set! Trash it! */
                        it = nodeSetList.erase(it);
                        it--; //Will be incremented by outer for loop
                        break;
                    }
                }
            }
        }

        bool AnalyseBitstream::setsIntersect(const t_rr_set& set1, const t_rr_set& set2) {
            for (const t_rr_idx& idx1 : set1) {
                if (set2.count(idx1))
                    return true;
            }

            return false;
        }

        t_rr_set
            AnalyseBitstream::traceOpinsToIpins(const std::vector<t_rr_idx>& opins,
                                                const PBPinLookupSet& pin_lookup_set,
                                                const BitstreamBlocks& blocks,
                                                const SwizzleData& swizzle,
                                                TracedDevice& td) {
            t_rr_set ipins;

            std::cout << "-- Tracing RR opins to ipins: ";

            for (t_rr_idx idx : opins)
                std::cout << idx << ",";

            std::cout << std::endl;

            auto& device = g_vpr_ctx.device();
            auto& clustering = g_vpr_ctx.clustering();
            const ClusteredNetlist& nlist = clustering.clb_nlist;

            std::map<ClusterBlockId, std::vector<int>> trace_list;
            std::map<ClusterBlockId, t_rr_node*> cluster_nodes;

            auto trace_pin = [&](ClusterBlockId cluster_id, ClusterPinId pin_idx) {
                std::cout << "-- Tracing pin index " << pin_idx.data() << " for clb " << cluster_id.data() << std::endl;
                auto it = trace_list.find(cluster_id);
                if (it == trace_list.end()) {
                    trace_list.emplace(cluster_id,
                                        std::vector<int> {pin_idx.data()});
                } else {
                    it->second.push_back(pin_idx.data());
                }
            };

            // Build the initial work list from the set of input pins
            for (t_rr_idx opin_rr_idx_unswz : opins) {
                t_rr_idx opin_rr_idx = swizzle.rr_device_to_block(opin_rr_idx_unswz);
                t_rr_node& node = device.rr_nodes[opin_rr_idx];

                std::cout << "-- Unswizzled RR: " << opin_rr_idx << std::endl;
                std::cout << "-- Processing RR: " << opin_rr_idx << std::endl;
                std::cout << "-- (Node @" << node.xlow() << ","
                            << node.ylow() << ":" << node.type_string()
                            << ")" << std::endl;

                ClusterBlockId clb_id = node.getCLBId();

                std::cout << "-- on CLB id " << clb_id.data() << std::endl;

                cluster_nodes.emplace(clb_id, &node);

                if (clb_id == ClusterBlockId::INVALID()) {
                    std::cout << "W Couldn't find CLB ID for RR OPIN "
                                << opin_rr_idx << std::endl;
                    continue;
                }

                if (node.ptc_num() == -1) {
                    std::cout << "W couldn't find pin ID for CLB ID "
                                << clb_id.data() << " (RR OPIN " << opin_rr_idx
                                << ")" << std::endl;
                    continue;
                }

                // FIXME: Add assertion that havingId != nullptr
                if (blocks.findTLBHavingId(clb_id.data())
                            ->disposition.count(BitstreamTLBDisposition::INPUT)) {
                    std::cout << "-- Trace hit FPGA input in CLB id: " << clb_id.data()
                                << "! Teriminating trace." << std::endl;
                    td.markRRasInput(opin_rr_idx, nlist.block_pb(clb_id));
                    continue;
                }

                td.addBlockPintoRR(clb_id.data(), node.ptc_num(), opin_rr_idx_unswz);
                trace_pin(clb_id, ClusterPinId(node.ptc_num()));
            }

            // For each cluster in the work list;
            for (auto& cluster_list : trace_list) {
                ClusterBlockId clb_id = cluster_list.first;
                std::vector<int>& cluster_work_pins = cluster_list.second;
                const t_rr_node& node = *cluster_nodes.find(clb_id)->second;
                const BlockPBPinLookup& pb_pin_lookup = pin_lookup_set.find(clb_id)->second;

                ClusteredNetlist::pin_range clb_in_rng =
                                                nlist.block_input_pins(clb_id);
                int low_input_idx = nlist.pin_physical_index(*clb_in_rng.begin());
                int hi_input_idx = nlist.pin_physical_index(*clb_in_rng.end());

                std::cout << "-- Now attacking work list; clb: " << clb_id.data()
                            << " with input_idx[" << hi_input_idx << ":"
                            << low_input_idx << "]" << std::endl;

                auto is_ipin = [&](int routing_pin) -> bool {
                    return routing_pin < hi_input_idx &&
                            routing_pin >= low_input_idx;
                };

                t_pb* pb = nlist.block_pb(clb_id);

                if (pb == nullptr) {
                    std::cout << "W couldn't find PB for CLB ID "
                                << clb_id.data() << std::endl;
                    continue;
                }

                if (pb->pb_route == nullptr) {
                    std::cout << "W PB has null routing!" << std::endl;
                    continue;
                }

                std::set<int> done_work_pins;

                // Work on the pins in the work list for the cluster
                while (cluster_work_pins.size()) {
                    std::vector<int> cluster_new_work_pins{};


                    for (int work_pin : cluster_work_pins) {
                        std::cout << "-- Doing traceback for pin " << work_pin
                                    << std::endl;

                        // Don't attempt to work on the same pin twice
                        if (done_work_pins.count(work_pin))
                            continue;

                        done_work_pins.insert(work_pin);

                        std::cout << "--\tTracing " << work_pin << std::endl;

                        t_pb_route* routeEdge = &pb->pb_route[work_pin];
                        int route_pin = work_pin;

                        if (routeEdge->driver_pb_pin_id == OPEN) {
                            std::cout << "W Traced opin CLB:pb_pin " << clb_id.data()
                                        << ":" << work_pin << " had no driver!"
                                        << " Traceback terminated."
                                        << std::endl;
                            continue;
                        }

                        // Follow the trace as far as it goes
                        while (routeEdge && routeEdge->driver_pb_pin_id != OPEN) {
                            std::cout << "--\tFollow " << route_pin << " <- " << routeEdge->driver_pb_pin_id << std::endl;
                            route_pin = routeEdge->driver_pb_pin_id;
                            routeEdge = &pb->pb_route[route_pin];
                        }

                        td.addBlockPintoPin(clb_id.data(), route_pin, work_pin);

                        /*
                         * There are now 2 possibilities for the state of route_pin:
                         * 1. It refers to an IPIN for the CLB. Great! Job done.
                         * 2. It refers to the output of a subblock within the CLB.
                         *    We need to keep tracing in order to determine the input
                         *    pins implicated by this block.
                         */
                        if (is_ipin(route_pin)) {
                            int ipin_offset = nlist.pin_physical_index(ClusterPinId(route_pin));
                            // FIXME: OH WOW PLEASE BOUNDS CHECK THIS MONSTROSITY
                            // Assume z-index is 0
                            t_rr_idx ipin_idx_swz = device.rr_node_indices[IPIN][node.xlow()][node.ylow()][0][ipin_offset];
                            t_rr_idx ipin_idx = swizzle.rr_block_to_device(ipin_idx_swz);

                            td.addRRtoBlockPin(ipin_idx, clb_id.data(), work_pin);
                            ipins.insert(ipin_idx);
                            std::cout << "-- Traceback RESULT: " << clb_id.data()
                                        << ":" << work_pin << " <-- "
                                        << route_pin << " (RR: " << ipin_idx << ")."
                                        << std::endl;
                        } else {
                            auto it = pb_pin_lookup.find(route_pin);
                            if (it == pb_pin_lookup.end()) {
                                std::cout << "W Missing pin when routing in CLB "
                                            << clb_id.data() << " to CLB pb pin "
                                            << work_pin << " (terminated at "
                                            << route_pin << ")." << std::endl;
                                continue;
                            }

                            t_pb* prim_pb = it->second;
                            if (!prim_pb->is_primitive()) {
                                std::cout << "-- Traced into dead-end non-primitive"
                                            << "; abandon route" << std::endl;
                                continue;
                            }

                            t_pb_graph_node* prim_gnode = prim_pb->pb_graph_node;

                            std::cout << "-- Reached primitive: "
                                        << prim_pb->name << std::endl;

                            std::vector<int> primitive_inputs {};

                            for (int i = 0; i < prim_gnode->num_input_ports; i++) {
                                for (int j = 0; j < prim_gnode->num_input_pins[i]; j++) {
                                    t_pb_graph_pin* ipin = &prim_gnode->input_pins[i][j];
                                    // FIXME OH GOD NO DON'T DO THIS TWICe
                                    primitive_inputs.push_back(ipin->pin_count_in_cluster);
                                    cluster_new_work_pins.push_back(ipin->pin_count_in_cluster);
                                }
                            }

                            td.addBlockPrimitive(clb_id.data(), prim_pb, work_pin, primitive_inputs);
                        }
                    }

                    std::cout << "--\tSpawned " << cluster_new_work_pins.size() << ":";

                    for (int pin : cluster_new_work_pins)
                        std::cout << pin << ",";

                    std::cout << std::endl;

                    // The new set of pins we've found to trace becomes the current work list.
                    std::swap(cluster_work_pins, cluster_new_work_pins);
                }

                std::cout << "-- Exhausted traceback set." << std::endl;
            }

            std::cout << "-- Exhausted all traceback sets." << std::endl
                        << "-- Traceback result set has RR nodes: [";
            for (t_rr_idx rr : ipins)
                std::cout << rr << ",";

            std::cout << "]." << std::endl;

            return ipins;
        }

        void AnalyseBitstream::build_pb_pin_lookup(t_pb* pb,
                                                    BlockPBPinLookup& lookup) {
            t_pb_graph_node* gnode = pb->pb_graph_node;
            const t_mode& mode = gnode->pb_type->modes[pb->mode];

            for (int i = 0; i < gnode->num_output_ports; i++) {
                for (int j = 0; j < gnode->num_output_pins[i]; j++) {
                    t_pb_graph_pin* opin = &gnode->output_pins[i][j];
                    std::cout << "--\tFound pb for opin " << opin->pin_count_in_cluster
                                << "("<< pb->name << ")" << std::endl;
                    lookup.emplace(opin->pin_count_in_cluster, pb);
                }
            }

            for (int i = 0; i < gnode->num_input_ports; i++) {
                for (int j = 0; j < gnode->num_input_pins[i]; j++) {
                    t_pb_graph_pin* ipin = &gnode->input_pins[i][j];
                    std::cout << "--\tFound pb for ipin " << ipin->pin_count_in_cluster
                                << "("<< pb->name << ")" << std::endl;
                    lookup.emplace(ipin->pin_count_in_cluster, pb);
                }
            }
        }

        void AnalyseBitstream::filterInvalidNodes(t_rr_setList& list) {
            for (auto it = list.begin(); it != list.end(); it++) {
                filterInvalidNodes(*it);

                if (!it->size()) {
                    it = list.erase(it);
                    it--;
                }
            }
        }

        void AnalyseBitstream::filterInvalidNodes(t_rr_set& set) {
            auto it = set.find(INVALID_IDX);

            if (it != set.end())
                set.erase(it);
        }
    }

    std::ostream& operator<<(std::ostream& os, const t_rr_set& nodeSet) {
        os << "nS{";

        for (const t_rr_idx& idx : nodeSet)
            os << idx << ",";

        return os << "}";
    }

    std::ostream& operator<<(std::ostream& os, const t_rr_setList& list) {
        os << "NS[" << std::endl;

        for (const t_rr_set& set : list)
            os << "\t" << set << std::endl;

        return os << "]" << std::endl;
    }
}

FABRIC_REGISTER_STAGE(AnalyseBitstream)
