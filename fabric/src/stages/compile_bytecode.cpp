#include "compile_bytecode.h"

#include "globals.h"

#include <algorithm>

namespace fabric {
    namespace Stages {
        void CompileBytecode::run(fabric::FabricInstance *fab) {
            ComputationGraph& g = fab->getComputationGraph();
            ExecutionGraph e(g);
            InstructionList& l = fab->getInstructionList();
            MemoryMap& m = fab->getMemoryMap();
            const BitstreamBlocks& blocks = fab->getBlocks();

            buildMemoryMap(g, e, m);
            buildInstructionList(g, e, l, m, blocks.primitiveBlocks);

            printMemoryMap(m);
            printInstrList(l);
        }

        void CompileBytecode::printInputs(ComputationGraph& cg, ExecutionGraph& eg) {
            const std::vector<t_cg_idx>& ex_nodes = eg.nodes();

            for (int i = 0; i < ex_nodes.size(); i++) {
                CGNode& n = cg.node(ex_nodes[i]);

                std::cout << "[" << i << "] (" << ex_nodes[i] << "):";

                if (n.prim_idx == -1)
                    std::cout << "IO: " << n.pb->name << std::endl;
                else {
                    std::cout << "Prim:" << n.prim_idx << ":" << n.pb->name;
                    std::cout << std::endl << "\tsrc: ";

                    for (t_cg_idx cg : n.sources)
                        std::cout << cg << ",";

                    std::cout << std::endl << "\tsrcAddr: ";

                    for (t_cg_idx cg : n.sources)
                        std::cout << eg.lookup(cg) << ",";

                    std::cout << std::endl;
                }
            }
        }

        void CompileBytecode::printInstrList(const InstructionList& l) {
            std::cout << "-- Instruction Listing:" << std::endl;

            for (const auto& i : l)
                std::cout << "-- " << i << std::endl;
        }

        void CompileBytecode::printMemoryMap(const MemoryMap& m) {
            std::cout << "-- Memory Map:" << std::endl;

            for (const auto& p : m)
                std::cout << "-- (" << p.first << "): %" << p.second << std::endl;
        }

        void CompileBytecode::buildMemoryMap(ComputationGraph& g,
                                               ExecutionGraph& e,
                                                MemoryMap& memoryMap) {
            int nextAddr = 2; // Reserve addrs 0 and 1 for constant 0 and 1 respectively

            std::vector<t_cg_idx> output_patch_nodes;

            for (t_cg_idx cg_node : e.nodes()) {
                CGNode& n = g.node(cg_node);

                if (n.prim_idx < 0) {
                    if (n.sources.size() == 0) {
                        // Net has no sources: is input. Allocate memory
                        memoryMap.emplace(n.cg_idx, nextAddr++);
                    } else { // Is output. Do not allocate memory
                        // Push for processing after all other nodes determined
                        output_patch_nodes.push_back(n.cg_idx);
                    }

                    continue;
                }

                // Is primitive.
                memoryMap.emplace(n.cg_idx, nextAddr++);
            }

            for (t_cg_idx out_idx : output_patch_nodes) {
                // Store output source address in map for output node
                CGNode& n = g.node(out_idx);
                memoryMap.emplace(n.cg_idx, memoryMap.find(*n.sources.begin())->second);
            }
        }

        void CompileBytecode::buildInstructionList(ComputationGraph& g,
                                                    ExecutionGraph& e,
                                                    InstructionList& l,
                                                    const MemoryMap& memoryMap,
                                                    const PBPrimitiveMap& blockMap) {
            for (t_cg_idx cg_node : e.nodes()) {
                CGNode& n = g.node(cg_node);

                if (n.prim_idx < 0)
                    continue;

                int dest_addr = memoryMap.find(cg_node)->second;
                std::vector<int> srcAddrs;

                std::transform(n.sources.begin(), n.sources.end(),
                                std::back_inserter(srcAddrs),
                                [&] (t_cg_idx src) -> int {
                    return memoryMap.find(src)->second;
                });

                auto& cluster_ctx = g_vpr_ctx.mutable_clustering();
                ClusteredNetlist& nlist = cluster_ctx.clb_nlist;

                l.emplace_back(dest_addr, cg_node, n.pb, srcAddrs);
                l.rbegin()->setPrimitive(&blockMap.find(n.pb)->second);
            }
        }
    }
}

FABRIC_REGISTER_STAGE(CompileBytecode)
