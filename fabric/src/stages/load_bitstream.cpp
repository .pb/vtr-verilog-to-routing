#include "load_bitstream.h"

// FABrIC headers
#include "fabric.h"
#include "exceptions.h"
#include "hash.h"
#include "swizzle_data.h"

// std headers
#include <sstream>
#include <string>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
#include <errno.h>

// libarchfpga headers
#include "read_xml_arch_file.h"
#include "arch_error.h"

// vtr headers
#include "vtr_error.h"

// vpr headers
#include "globals.h"
#include "rr_graph_reader.h"
#include "vpr_types.h"
#include "vpr_api.h"
#include "vpr_error.h"
#include "SetupVPR.h"
#include "SetupGrid.h"
#include "pb_type_graph.h"
#include "vpr_utils.h"
#include "clustered_netlist_fwd.h"
#include "clustered_netlist.h"
#include "vpr_context.h"

// misc. headers
#include "pugixml.hpp"

#define FABRIC_EXPT_BITSTREAM_FMT "1.5"

namespace fabric {
    namespace Stages {
        void LoadBitstream::run(fabric::FabricInstance *fab) {
            pugi::xml_document& bstmDoc = fab->getBitstream();
            t_vpr_setup& vpr_setup = fab->getVprSetup();
            loadBitstreamFile(fab->getBitstreamFilename(), bstmDoc);

            pugi::xml_node n_bitstream = bstmDoc.child("bitstream");
            pugi::xml_node n_routing = n_bitstream.child("routing");
            pugi::xml_node n_arch = n_bitstream.child("architecture");
            pugi::xml_node n_placement= n_bitstream.child("placement");
            pugi::xml_node n_swizzle = n_bitstream.child("swizzle");

            const char* formatVersion = n_bitstream.attribute("format").as_string();
            if (strcmp(formatVersion, FABRIC_EXPT_BITSTREAM_FMT)) {
                std::cout << "! Bitstream format (" << formatVersion
                            << ") is different to " << FABRIC_EXPT_BITSTREAM_FMT
                            << ". Everything might go terribly."
                            << std::endl;
            } else {
                std::cout << "* Processing bitstream file version "
                            << FABRIC_EXPT_BITSTREAM_FMT
                            << "." << std::endl;
            }

            t_arch& architecture = fab->getArchitecture();
            BitstreamBlocks& blocks = fab->getBlocks();

            loadArch(n_arch, architecture, vpr_setup);
            loadDeviceGrid(n_arch, architecture, vpr_setup, blocks);
            fab->setSourceLookup(loadRR(n_arch, architecture, vpr_setup));
            parseRouting(n_routing, fab->getBstm_sbits());
            loadBlocks(n_placement, blocks);
            loadSwizzle(n_swizzle, fab->getSwizzle());
        }

        void LoadBitstream::loadBitstreamFile(const std::string& filename,
                                                pugi::xml_document& bstmDoc) {
            char* bitstreamFilename = strdup(filename.c_str());
            pugi::xml_parse_result result = bstmDoc.load_file(bitstreamFilename);

            if (!result) {
                std::stringstream ss;
                ss << "Bitstream parse failed: "
                    << result.description();
                free(bitstreamFilename);
                throw InputException(ss.str());
            }

            const char* bitstreamDir = dirname(bitstreamFilename);

            if (chdir(bitstreamDir)) {
                std::stringstream ss;
                ss << "Failed to chdir to " << bitstreamDir
                    << " from " << getcwd(nullptr, 0) // FIXME: not posix, memleak, generally terrible
                    << " - " << strerror(errno);
                free(bitstreamFilename);
                throw GeneralException(ss.str());
            }

            free(bitstreamFilename);
        }

        void LoadBitstream::loadArch(const pugi::xml_node& arch,
                                        t_arch& architecture,
                                        t_vpr_setup& vpr_setup) {
            const char* archFile = arch.attribute("origin").as_string();
            auto& device_ctx = g_vpr_ctx.mutable_device();

            try {
                XmlReadArch(archFile,
                            false,
                            &architecture,
                            &device_ctx.block_types,
                            &device_ctx.num_block_types);

                /*
                 * Store a copy for VPR, as per vpr_create_device_grid.
                 * FIXME: Now there are two copies of t_arch - this is
                 * pretty poor, one here and one in the FabricInstance.
                 * Do we actually need a copy in device_ctx?
                 */
                device_ctx.arch = architecture;
                std::cout << "* Loaded architecture file [" << archFile << "]" << std::endl;
            } catch (ArchFpgaError& e) {
                std::stringstream ss;
                ss << "Architecture file [" << archFile << "] parse failed: " << e.what();

                throw InputException(ss.str());
            } catch (vtr::VtrError& e) { // Will be thrown in secure_digest_file if file can't be opened
                std::stringstream ss;
                ss << "Architecture file [" << archFile << "] load failed: " << e.what();

                throw InputException(ss.str());
            }

            try {
                SetupVPR_archTypes(&architecture,
                                    &vpr_setup.user_models,
                                    &vpr_setup.library_models,
                                    &vpr_setup.Segments,
                                    &vpr_setup.RoutingArch);

                SetupVPR_switches(architecture,
                                    &vpr_setup.RoutingArch,
                                    architecture.Switches,
                                    architecture.num_switches);

                SetupVPR_routingArch(architecture,
                                        &vpr_setup.RoutingArch);

                std::cout << "* Configured architecture" << std::endl;
            } catch (vtr::VtrError& e) {
                std::stringstream ss;
                ss << "Architecture configuration failed: " << e.what();
                throw InputException(ss.str());
            }
        }

        void LoadBitstream::loadDeviceGrid(const pugi::xml_node& n_arch,
                                            const t_arch& architecture,
                                            t_vpr_setup& vpr_setup,
                                            BitstreamBlocks& blocks) {
            // Essentially a parallel of vpr_create_device_grid but more generic
            auto& device_ctx = g_vpr_ctx.mutable_device();

            const pugi::xml_node& n_dims = n_arch.child("dimension");
            int width = n_dims.attribute("x").as_int();
            int height = n_dims.attribute("y").as_int();

            const pugi::xml_node& n_layout = n_arch.child("layout");
            std::string layout = n_layout.attribute("name").as_string();
            vpr_setup.device_layout = layout; //FIXME: Validate

            device_ctx.grid = create_device_grid(layout,
                                                    architecture.grid_layouts,
                                                    width,
                                                    height);

            vpr_set_all_channel_width_bounds(0, 0); // FIXME: Should this be set to the channel width?
            vpr_allocate_device_channel_lists();
            blocks.dims.x = width;
            blocks.dims.y = height;
        }

        rr_source_lookup LoadBitstream::loadRR(const pugi::xml_node& n_arch,
                                                const t_arch& architecture,
                                                t_vpr_setup& vpr_setup) {
            const std::string rrFile(n_arch.attribute("rr-graph").as_string());

            if (rrFile.empty())
                throw InputException("No rr-file specified in <architecture>!");

            const pugi::xml_node& n_channels = n_arch.child("channels");
            int chan_width = n_channels.attribute("width").as_int(); //FIXME: Validate
            // We're interested in detailled routing, not just global
            vpr_setup.RouterOpts.route_type = DETAILED;

            // We don't care, but gets read in rr_indexed_data init
            vpr_setup.RouterOpts.base_cost_type = DEMAND_ONLY;

            // We only simulate directional FPGAs
            vpr_setup.RoutingArch.directionality = UNI_DIRECTIONAL;

            // Instruct create_rr_graph to read in the rr graph named in this file
            vpr_setup.RoutingArch.read_rr_graph_filename = rrFile;

            /*
             * The following options are read but don't need setting:
             * vpr_setup.RoutingArch.wire_to_rr_ipin_switch - is derefed and set during rr build
             * vpr_setup.RoutingArch.num_segment - set in SetupVPR_archTypes
             * vpr_setup.Segments - set in SetupVPR_archTypes
             */

            try {
                vpr_create_rr_graph(vpr_setup, architecture, chan_width);
            } catch (VprError& e) {
                std::stringstream ss;
                ss << "RR Graph [" << rrFile << "] load failed: " << e.what();
                throw InputException(ss.str());
            }

            auto& device_ctx = g_vpr_ctx.device();
            rr_source_lookup lookup(device_ctx.rr_nodes, device_ctx.num_rr_nodes);

            std::cout << "* Loaded RR graph [" << rrFile << "]" << std::endl;

            return lookup;
        }

        void LoadBitstream::parseRouting(const pugi::xml_node& routing,
                                            std::set<t_rr_switchbit_id>& sbits) {
            if (!routing)
                throw InputException("Missing <routing> in bitstream");

            std::stringstream ss(routing.text().as_string());
            t_rr_switchbit_id sbit;
            int i = 0;

            while (ss >> sbit) {
                sbits.insert(sbit);

                if (ss.peek() == ',')
                    ss.ignore();

                i++;
            }

            std::cout << "* Processed " << i << " enabled switch-bits." << std::endl;
        }

        void LoadBitstream::loadBlocks(const pugi::xml_node& placement,
                                        BitstreamBlocks& blocks) {
            int proc_io = 0,
                proc_clb = 0,
                proc_clbsub = 0,
                proc_iosub = 0;

            for (pugi::xml_node& blk_n : placement.children("tlb")) {
                const char* disp = blk_n.attribute("disposition").as_string();

                if (!disp) {
                    std::cout << "W <tlb> missing disposition - will be ignored."
                                << std::endl;
                    continue;
                }

                const TLBDispositionSet dispositions = loadDispositions(disp);

                if (dispositions.count(BitstreamTLBDisposition::CLB)) {
                        proc_clb++;
                        proc_clbsub += loadBlockTLB(blk_n, dispositions, blocks);
                } else if (dispositions.count(BitstreamTLBDisposition::IO)) {
                    proc_io++;
                    proc_iosub += loadBlockTLB(blk_n, dispositions, blocks);
                }  else {
                    std::cout << "W <tlb> has disposition "
                                << disp << " which contains neither io nor clb. "
                                << "This <tlb> will be ignored.";
                    continue;
                }
            }

            alloc_and_load_all_pb_graphs(true, true);
            sync_grid_to_blocks();

            std::cout << "* Processed "
                        << proc_io + proc_clb + proc_clbsub + proc_iosub
                        << " blocks, including "
                        << proc_io << " IO blocks, "
                        << proc_iosub << " IO sub-blocks, "
                        << proc_clb << " CLBs and "
                        << proc_clbsub << " CLB sub-blocks."
                        << std::endl;
        }

        const TLBDispositionSet LoadBitstream::loadDispositions(const std::string& dispStr) {
            TLBDispositionSet dispSet{};
            char* dispCStr = strdup(dispStr.c_str());

            for (char* strPtr = strtok(dispCStr, ",");
                    strPtr != nullptr;
                    strPtr = strtok(nullptr, ",")) {
                BitstreamTLBDisposition disp;

                switch (hash_djb2(strPtr)) {
                    case "input"_hash:
                        disp = BitstreamTLBDisposition::INPUT;
                        break;

                    case "output"_hash:
                        disp = BitstreamTLBDisposition::OUTPUT;
                        break;

                    case "clock"_hash:
                        disp = BitstreamTLBDisposition::CLOCK;
                        break;

                    case "io"_hash:
                        disp = BitstreamTLBDisposition::IO;
                        break;

                    case "clb"_hash:
                        disp = BitstreamTLBDisposition::CLB;
                        break;

                    default:
                        std::cout << "W Ignored unknown disposition "
                                    << strPtr << std::endl;
                        continue;
                }

                dispSet.insert(disp);
            }

            free(dispCStr);

            return std::move(dispSet);
        }

        int LoadBitstream::loadBlockTLB(const pugi::xml_node& blk_n,
                                        const TLBDispositionSet& dispositions,
                                        BitstreamBlocks& blocks) {
            BitstreamTLB tlb;
            if (dispositions.count(BitstreamTLBDisposition::IO)) {
                tlb.coord = BitstreamCoord(blk_n.attribute("x").as_int(),
                                           blk_n.attribute("y").as_int(),
                                           blk_n.attribute("z").as_int());
            } else {
                tlb.coord = BitstreamCoord(blk_n.attribute("x").as_int(),
                                           blk_n.attribute("y").as_int());
            }

            tlb.disposition = std::move(dispositions);
            tlb.id = blk_n.attribute("id").as_int();
            tlb.name = blk_n.attribute("name").as_string();

            BitstreamBlock& block = static_cast<BitstreamBlock&>(tlb);

            int children = loadBlockGeneral(blk_n, block, blocks);
            block.index = tlb.id; // TLB indexes are always 0, which isn't helpful for indexing them
            blocks.blocks.addBlock(std::move(tlb));

            return children;
        }

        int LoadBitstream::loadBlockSubblock(const pugi::xml_node& blk_n,
                                                ChildBlockLookup& destination,
                                                BitstreamBlocks& blocks) {
            BitstreamBlock block;
            int children = 1 + loadBlockGeneral(blk_n, block, blocks);
            std::stringstream ss;
            ss << "?fbc?" << blk_n.attribute("instance").as_string();
            block.name = ss.str();

            destination.addBlock(std::move(block));

            return children;
        }

        int LoadBitstream::loadBlockGeneral(const pugi::xml_node& blk_n,
                                            BitstreamBlock& block,
                                            BitstreamBlocks& blocks) {
            int children = 0;

            const std::string typeName = blk_n.attribute("type").as_string();

            if (!typeName.size()) {
                std::cout << "W Block with instance '"
                            << blk_n.attribute("instance").as_string()
                            << " ' and name '"
                            << blk_n.attribute("name").as_string()
                            << " has no type, and will be ignored!"
                            << std::endl;
                return children;
            }

            block.bstmBlockType = blocks.blockTypes.lookup(typeName);
            block.index = blk_n.attribute("index").as_int();
            block.mode = 0; // Initialise to default mode, will be overridden below if configurable

            for (pugi::xml_node& child_n : blk_n.children()) {
                switch (hash_djb2(child_n.name())) {
                    case "interconnect"_hash:
                        processBlockInterconnect(child_n, block);
                        break;

                    case "block"_hash:
                        children += loadBlockSubblock(child_n, block.children, blocks);
                        break;

                    case "mode"_hash:
                        block.mode = parseHexString(child_n.text().as_string());
                        break;

                    case "names"_hash:
                    case "model"_hash:
                    case "ff"_hash:
                        block.primitive.push_back(
                                BitstreamPrimitive(child_n.name(),
                                                    child_n.text().as_string()));
                        break;

                    default:
                        std::cout << "W Unexpected block child <"
                                    << child_n.name()
                                    << "> in <"
                                    << blk_n.name()
                                    << ">!"
                                    << std::endl;
                }
            }

            return children;
        }

        void LoadBitstream::processBlockInterconnect(const pugi::xml_node& iconset_n, BitstreamBlock& block) {
            for (const pugi::xml_node& icon_n : iconset_n.children()) {
                int width = icon_n.attribute("width").as_int();
                BitstreamInterconnectType type;

                switch (hash_djb2(icon_n.name())) {
                    case "complete"_hash:
                        type = BitstreamInterconnectType::COMPLETE;
                        break;

                    case "mux"_hash:
                        type = BitstreamInterconnectType::MUX;
                        break;

                    default:
                        std::cout << "Unknown interconnect type <"
                                    << icon_n.name()
                                    << ">! Will ignore."
                                    << std::endl;
                        continue;
                }

                BitstreamBlockInterconnect icon(icon_n.attribute("name").as_string(),
                                                width,
                                                type);

                for (const pugi::xml_node& out_n : icon_n.children("out")) {
                    int selIndex = parseHexString(out_n.text().as_string());
                    icon.ways.push_back(BitstreamInterconnectWay(
                                    out_n.attribute("id").as_int(),
                                    selIndex
                                ));

                    if (selIndex >= width) {
                        std::cout << "W Interconnect " << icon_n.name()
                                    << ":" << icon_n.attribute("name").as_string()
                                    << " [width " << width << "] way "
                                    << out_n.attribute("id").as_int()
                                    << " has selection " << selIndex
                                    << " which is larger than its width!"
                                    << std::endl;
                    }
                }

                block.interconnect.push_back(icon);
            }
        }

        int LoadBitstream::parseHexString(const std::string& hex) {
            if (hex.size() < 3)
                throw InputException("Unexpectedly short hex value!");

            if (hex.substr(0, 2).compare("0x"))
                throw InputException("Hex string didn't start with 0x delimiter!");

            return std::stoi(hex, nullptr, 16);
        }

        void LoadBitstream::loadSwizzle(const pugi::xml_node& swizzle_n, SwizzleData& swz) {
            for (const pugi::xml_node& n : swizzle_n.children("rr-map")) {
                swz.swizzle_rr(n.attribute("device").as_int(),
                                n.attribute("block").as_int());
            }
        }
    }
}

FABRIC_REGISTER_STAGE(LoadBitstream)
