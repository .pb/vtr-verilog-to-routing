#ifndef FABRIC_STAGES_H
#define FABRIC_STAGES_H

#include "load_bitstream.h"
#include "analyse_bitstream.h"
#include "generate_computation_graph.h"
#include "compile_bytecode.h"
#include "write_bytecode.h"

#endif
