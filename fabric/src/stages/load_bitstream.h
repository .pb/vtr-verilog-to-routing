#ifndef FABRIC_STAGE_LOAD_BITSTREAM_H
#define FABRIC_STAGE_LOAD_BITSTREAM_H

#include "stage.h"
#include "swizzle_data.h"

#include <iostream>

#include "bitstream_blocks.h"
#include "rr_source_lookup.h"

#include "rr_graph2.h"
#include "arch_types.h"
#include "vpr_types.h"
#include "pugixml.hpp"

namespace fabric {
    namespace Stages {
        class LoadBitstream : public StageBase {
        FABRIC_INIT_STAGE
        public:
            LoadBitstream() {}

            void run(fabric::FabricInstance *fab);

        protected:
            void loadBitstreamFile(const std::string& filename,
                                    pugi::xml_document& bstmDoc);
            void loadArch(const pugi::xml_node& arch,
                            t_arch& architecture,
                            t_vpr_setup& vpr_setup);
            void loadDeviceGrid(const pugi::xml_node& n_arch,
                                const t_arch& architecture,
                                t_vpr_setup& vpr_setup,
                                BitstreamBlocks& blocks);
            rr_source_lookup loadRR(const pugi::xml_node& n_arch,
                                    const t_arch& architecture,
                                    t_vpr_setup& vpr_setup);
            void parseRouting(const pugi::xml_node& routing,
                                std::set<t_rr_switchbit_id>& sbits);
            void loadBlocks(const pugi::xml_node& placement, BitstreamBlocks& blocks);

            int loadBlockTLB(const pugi::xml_node& blk_n,
                                const TLBDispositionSet& dispositions,
                                BitstreamBlocks& blocks);
            int loadBlockSubblock(const pugi::xml_node& blk_n,
                                    ChildBlockLookup& destination,
                                    BitstreamBlocks& blocks);
            int loadBlockGeneral(const pugi::xml_node& blk_n,
                                    BitstreamBlock& block,
                                    BitstreamBlocks& blocks);
            void processBlockInterconnect(const pugi::xml_node& icon_n,
                                            BitstreamBlock& block);

            int parseHexString(const std::string& hex);
            const TLBDispositionSet loadDispositions(const std::string& dispStr);

            void loadSwizzle(const pugi::xml_node& swizzle_n, SwizzleData& swz);
        };
    }
}

#endif
