#ifndef FABRIC_FABRIC_H
#define FABRIC_FABRIC_H

#include "stage.h"
#include "bitstream_blocks.h"
#include "rr_source_lookup.h"
#include "swizzle_data.h"
#include "traced_device.h"
#include "computation_graph.h"
#include "instruction.h"

#include <string>
#include <set>

#include "pugixml.hpp"
#include "rr_graph2.h"
#include "arch_types.h"
#include "vpr_types.h"

class StageList;

namespace fabric {
    class FabricInstance {
        public:
            FabricInstance():
                architecture() {
            }

            void run(StageList::StageId stageId);
            void setBitstreamFilename(const std::string& s);

            std::string& getBitstreamFilename() {
                return bitstreamFilename;
            }

            pugi::xml_document& getBitstream() {
                return bitstream;
            }

            std::set<t_rr_switchbit_id>& getBstm_sbits() {
                return bstm_sbits;
            }

            t_arch& getArchitecture() {
                return architecture;
            }

            t_vpr_setup& getVprSetup() {
                return vpr_setup;
            }

            BitstreamBlocks& getBlocks() {
                return bstm_blocks;
            }

            void setSourceLookup(rr_source_lookup&& lookup) {
                sourceLookup = lookup;
            }

            rr_source_lookup& getSourceLookup() {
                return sourceLookup;
            }

            SwizzleData& getSwizzle() {
                return swizzle_data;
            }

            TracedDevice& getTracedDevice() {
                return traced_device;
            }

            ComputationGraph& getComputationGraph() {
                return comp_graph;
            }

            InstructionList& getInstructionList() {
                return instr_list;
            }

            MemoryMap& getMemoryMap() {
                return mem_map;
            }

        protected:
            std::string bitstreamFilename;
            pugi::xml_document bitstream;
            std::set<t_rr_switchbit_id> bstm_sbits;
            BitstreamBlocks bstm_blocks;
            rr_source_lookup sourceLookup;
            SwizzleData swizzle_data;
            TracedDevice traced_device;
            ComputationGraph comp_graph;
            InstructionList instr_list;
            MemoryMap mem_map;

            t_arch architecture;
            t_vpr_setup vpr_setup;
    };
}

#endif
