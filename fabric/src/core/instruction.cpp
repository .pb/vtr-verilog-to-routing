#include "instruction.h"

namespace fabric {
    std::ostream& operator<<(std::ostream& os, const instruction& i) {
        os << "(" << i.cg_node << ")<" << i.prim_pb->name << ">: %"
            << i.dest_addr << "=";

        if (i.prim)
            os << i.prim->name << "{" << i.prim->configuration << "}";
        else
            os << "{prim-missing!}";

        os << "[";

        for (int a: i.srcAddrs)
            os << "%" << a << ",";

        return os << "]";
    }
}
