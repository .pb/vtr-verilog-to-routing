#ifndef FABRIC_BITSTREAM_BLOCKS_H
#define FABRIC_BITSTREAM_BLOCKS_H

#include "vpr_types.h"

#include <vector>
#include <map>
#include <string>
#include <set>
#include <iostream>

namespace fabric {
    struct BitstreamBlock;
    struct BitstreamTLB;

    typedef int BitstreamToplevelBlockId;
    typedef int BitstreamSubblockOffset;
    typedef int BitstreamBlockTypeIndex;

    enum class BitstreamTLBDisposition {
        INPUT,
        OUTPUT,
        CLOCK,
        IO,
        CLB
    };

    typedef std::set<BitstreamTLBDisposition> TLBDispositionSet;

    std::ostream& operator<<(std::ostream& os, BitstreamTLBDisposition disp);
    std::ostream& operator<<(std::ostream& os, TLBDispositionSet dispList);

    enum class BitstreamInterconnectType {
        COMPLETE,
        MUX
    };

    template <typename T>
    using BlockTypeSet = std::map<BitstreamSubblockOffset, T>;

    template <typename T>
    struct BlockLookup {
        BlockLookup():
            map(), size_(0) {
        }

        std::map<BitstreamBlockTypeIndex, BlockTypeSet<T>> map;

        void addBlock(T&& block);
        BlockTypeSet<T>& getTypeSet(BitstreamBlockTypeIndex typeIndex);

        std::vector<const T*> havingDisposition(BitstreamTLBDisposition d) const;

        int size() const { return size_; }
    protected:
        int size_;
    };

    typedef BlockLookup<BitstreamBlock> ChildBlockLookup;
    typedef BlockLookup<BitstreamTLB> TLBLookup;

    struct BlockTypeLookup {
        BlockTypeLookup():
            map_toIndex(), map_toString() {
        }

        BitstreamBlockTypeIndex lookup(std::string blockType) {
            auto typeIter = map_toIndex.find(blockType);

            if (typeIter == map_toIndex.end()) {
                map_toIndex.emplace(blockType, nextId);
                map_toString.emplace(nextId, blockType);

                return nextId++;
            } else {
                return typeIter->second;
            }
        }

        const char* lookup(BitstreamBlockTypeIndex blockTypeIdx) const {
            auto typeIter = map_toString.find(blockTypeIdx);

            if (typeIter == map_toString.end()) {
                return nullptr;
            }

            return typeIter->second.c_str();
        }

        int numTypes() const {
            return nextId;
        }

    protected:
        std::map<std::string, BitstreamBlockTypeIndex> map_toIndex;
        std::map<BitstreamBlockTypeIndex, std::string> map_toString;
        int nextId = 0;
    };

    struct BitstreamCoord {
        int x;
        int y;
        int z;

        BitstreamCoord(int xval, int yval):
            x(xval), y(yval), z(-1) {
        }

        BitstreamCoord(int xval, int yval, int zval):
            x(xval), y(yval), z(zval) {
        }

        BitstreamCoord():
            x(-1), y(-1), z(-1) {
        }
    };


    struct BitstreamPrimitive {
        std::string name;
        std::string configuration;

        BitstreamPrimitive(const std::string& primName,
                            const std::string& primConfig):
            name(primName), configuration(primConfig) {
            }
    };

    struct BitstreamInterconnectWay {
        int wayId;
        int selectedInputIndex;

        BitstreamInterconnectWay(int icon_wayId, int selIndex):
            wayId(icon_wayId), selectedInputIndex(selIndex) {
        }
    };

    struct BitstreamBlockInterconnect {
        std::string name;
        int inputWidth;
        std::vector<BitstreamInterconnectWay> ways;
        BitstreamInterconnectType type;

        BitstreamBlockInterconnect(
                std::string icon_name,
                int icon_width,
                BitstreamInterconnectType icon_type):
            name(icon_name), inputWidth(icon_width), type(icon_type) {
        }
    };

    struct BitstreamBlock {
        int mode;
        BitstreamBlockTypeIndex bstmBlockType;
        ChildBlockLookup children;
        std::vector<BitstreamBlockInterconnect> interconnect;
        std::vector<BitstreamPrimitive> primitive;
        BitstreamSubblockOffset index;
        std::string name;

        BitstreamBlock():
            mode(-1),
            bstmBlockType(),
            children(),
            interconnect(),
            primitive(),
            index(-1),
            name("UNKNOWN") {
        }
    };

    struct BitstreamTLB : BitstreamBlock {
        BitstreamCoord coord;
        BitstreamToplevelBlockId id;
        TLBDispositionSet disposition;
    };

    typedef std::map<t_pb*, BitstreamPrimitive> PBPrimitiveMap;

    struct BitstreamBlocks {
        BlockTypeLookup blockTypes;
        TLBLookup blocks;
        BitstreamCoord dims;
        PBPrimitiveMap primitiveBlocks;

        BitstreamBlocks():
            blockTypes(), blocks(), dims(), primitiveBlocks() {
        }

        const BitstreamTLB* findTLBHavingId(BitstreamToplevelBlockId id) const;
    };
}

#endif
