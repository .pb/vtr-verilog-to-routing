#include "traced_device.h"

#include "vpr_context.h"
#include "globals.h"

namespace fabric {
    bool operator<(const block_pin& first, const block_pin& other) {
        return first.clb_id < other.clb_id || first.pin_id < other.pin_id;
    }

    void TracedDevice::traceRRtoRR(t_rr_idx source, t_rr_idx sink) {
        rr_superedges_.emplace_back(source, sink);
        rr_superedge_src_lookup_.emplace(sink, rr_superedges_.size() - 1);
    }

    void TracedDevice::markRRasInput(t_rr_idx input, t_pb* pb) {
        input_rr_.push_back(input);
        io_pbs_.emplace(std::make_pair(input, pb));
    }

    void TracedDevice::markRRasOutput(t_rr_idx output, t_pb* pb) {
        outsink_rr_.emplace(output);
        io_pbs_.emplace(std::make_pair(output, pb));
    }

    void TracedDevice::markRRasOutput(const std::vector<t_rr_idx>& output,
                                        const std::vector<t_pb*>& pbs) {
        // FIXME: Do this less stoopidly
        for (int i = 0; i < output.size(); i++)
            markRRasOutput(output[i], pbs[i]);
    }

    void TracedDevice::nodeSets(t_rr_setList& nodeSets) {
        for (const t_rr_set& set : nodeSets) {
            std::vector<t_rr_idx> sinks{};
            t_rr_idx src = -1;

            // FIXME: We grab the pb from the last SINK node we processed,
            //          but because we've already been through set amalgamation
            //          we have no way of knowing which SINK corresponds to which
            //          IPIN, which means we can't assign the SINK's pb to the correct
            //          IPIN. This will mean that nets with multiple primary outputs
            //          only have one primary output pb actually assigned. This is bad.
            //          To fix this, store pb information during net globbing, so we trace
            //          which SINK the IPIN relates to.
            t_pb* POSSIBLY_INCORRECT_pb = nullptr;
            bool sinks_are_primary = false;

            for (const t_rr_idx& nodeIdx : set) {
                auto& device_ctx = g_vpr_ctx.device();
                if (device_ctx.rr_nodes[nodeIdx].type() == OPIN)
                    src = nodeIdx;
                else if (device_ctx.rr_nodes[nodeIdx].type() == IPIN)
                    sinks.push_back(nodeIdx);
                else if (device_ctx.rr_nodes[nodeIdx].type() == SINK &&
                        outsink_rr_.count(nodeIdx)) {
                    /* This set has primary outputs!
                     * FIXME: This is pretty hacky, should probably identify
                     * them properly in analysis instead */
                    sinks_are_primary = true;
                    POSSIBLY_INCORRECT_pb = io_pbs_.find(nodeIdx)->second;
                }
            }

            for (const t_rr_idx& nodeIdx : sinks) {
                traceRRtoRR(src, nodeIdx);

                if (sinks_are_primary) {
                    output_rr_.push_back(nodeIdx);
                    io_pbs_.emplace(std::make_pair(nodeIdx, POSSIBLY_INCORRECT_pb));
                }
            }
        }
    }

    void TracedDevice::addBlockPintoRR(int clb_id, int pin, t_rr_idx rr) {
        block_to_rr_.emplace(block_pin(clb_id, pin), rr);
        block_to_rr_inv_.emplace(rr, block_pin(clb_id, pin));
    }

    void TracedDevice::addRRtoBlockPin(t_rr_idx rr, int clb_id, int pin) {
        rr_to_block_.emplace(rr, block_pin(clb_id, pin));
        rr_to_block_inv_.emplace(block_pin(clb_id, pin), rr);
    }

    void TracedDevice::addBlockPintoPin(int clb_id, int source, int sink) {
        auto it = block_superedges_.find(clb_id);
        int src_idx = 0;

        if (it == block_superedges_.end()) {
            block_superedges_.emplace(clb_id,
                                        std::vector<block_super_edge>
                                            {block_super_edge(source, sink)});
        } else {
            it->second.emplace_back(source, sink);
            src_idx = it->second.size() - 1;
        }

        std::cout << "-- Added superedge: " << clb_id << ":" << source << "->" << sink
                    << " as ID " << src_idx << " for this clb." << std::endl;

        block_superedge_src_lookup_.emplace(block_pin(clb_id, sink), src_idx);
    }

    void TracedDevice::addBlockPrimitive(int clb_id,
                                            t_pb* pb,
                                            int output,
                                            const std::vector<int>& inputs) {
        primitives_.emplace_back(pb, clb_id, output, inputs);
        primitive_by_output_pin_.emplace(block_pin(clb_id, output), primitives_.size() - 1);
    }

    const std::vector<t_rr_idx>& TracedDevice::outputs() const {
        return output_rr_;
    }

    const std::vector<t_rr_idx>& TracedDevice::inputs() const {
        return input_rr_;
    }

    const primitive& TracedDevice::getPrimitive(int primitive_index) const {
        return primitives_[primitive_index];
    }

    t_pb* TracedDevice::io(t_rr_idx io) const {
        auto it = io_pbs_.find(io);

        if (it == io_pbs_.end())
            return nullptr;

        return it->second;
    }

    source_set TracedDevice::source_of_rr(t_rr_idx rr) const {
        source_set src;

        // An RR can either have an RR source (an input), or a Primitive.
        rr_source_subtrace(rr, src);

        auto blkit = block_to_rr_inv_.find(rr);

        if (blkit != block_to_rr_inv_.end()) {
            block_pin pin = blkit->second;
            src.primitives.push_back(primitive_by_output_pin_.find(pin)->second);
        }

        return src;
    }

    source_set TracedDevice::source_of_primitive(int primitive_index) const {
        source_set src;

        if (primitive_index < 0) {
            std::cout << "W Attempted trace of invalid primitive" << std::endl;
            return src;
        }

        const primitive& prim = primitives_[primitive_index];

        // A primitive's N inputs can have...
        for (int pin : prim.source_pins) {
            block_pin srcPin(prim.clb_id, pin);
            auto to_rr_it = rr_to_block_inv_.find(srcPin);

            // ... An RR source which is an input, or a link to another primitve, or...
            if (to_rr_it != rr_to_block_inv_.end()) {
                t_rr_idx rr = to_rr_it->second;
                rr_source_subtrace(rr, src);
            }

            // ... a different primitive within the same block:
            auto block_it = block_superedge_src_lookup_.find(srcPin);

            if (block_it != block_superedge_src_lookup_.end()) {
                auto this_block_superedges_it = block_superedges_.find(srcPin.clb_id);

                if (this_block_superedges_it == block_superedges_.end()) {
                    std::cout << "W Recorded a source superedge but no superedges found "
                                << "for CLB. Inconsistent!" << std::endl;
                    continue;
                }

                // FIXME We're not consistent as to whether we care about bsedges
                // Sometimes we store traced edges in sinks,sources, sometimes not
                const block_super_edge& bsedge = this_block_superedges_it
                                                    ->second[block_it->second];
#if 0
                int block_edge_source_pin = bsedge.source_pin;
#else
                int block_edge_source_pin = bsedge.sink_pin;
#endif
                block_pin output_pin(srcPin.clb_id, block_edge_source_pin);
                auto prim_it = primitive_by_output_pin_.find(output_pin);

                if (prim_it != primitive_by_output_pin_.end()) {
                    int prim_id = prim_it ->second;
                    src.primitives.push_back(prim_id);
                }
            }
        }

        return src;
    }

    void TracedDevice::rr_source_subtrace(t_rr_idx rr, source_set& src) const {
        auto rr_superedge_it = rr_superedge_src_lookup_.find(rr);

        if (rr_superedge_it != rr_superedge_src_lookup_.end()) {
            t_rr_idx rr_source = rr_superedges_[rr_superedge_it->second].source;

            auto block_it = block_to_rr_inv_.find(rr_source);

            if (block_it != block_to_rr_inv_.end()) {
                // This is actually a rr- > prim route
                block_pin bpin = block_it->second;
                // FIXME: This could probably be refactored from source_of_rr
                src.primitives.push_back(primitive_by_output_pin_
                                            .find(bpin)->second);
            } else {
                // This is a rr(input)
                src.input_sources.push_back(rr_source);
            }
        }
    }

    std::vector<t_rr_idx>
        filterNodeSetList(const t_rr_setList& setList,
                                            t_rr_type rr_type) {
        auto& device_ctx = g_vpr_ctx.device();
        std::vector<t_rr_idx> retVec{};

        for (const t_rr_set& nodeSet : setList) {
            for (const t_rr_idx& nodeIdx : nodeSet) {
                if (device_ctx.rr_nodes[nodeIdx].type() == rr_type) {
                    retVec.push_back(nodeIdx);
                }
            }
        }

        return retVec;
    }

    std::vector<t_rr_idx>
        filterNodeList(const std::vector<t_rr_idx>& nodeList,
                                            const t_rr_set& filterSet) {
        std::vector<t_rr_idx> retVec{};

        for (const t_rr_idx& idx : nodeList) {
            if (!filterSet.count(idx))
                retVec.push_back(idx);
        }

        return retVec;
    }

    std::ostream& operator<<(std::ostream& os, const source_set& ss) {
        os << "SourceSet: [" << std::endl
            << "\tinput-rrs: [";

        for (t_rr_idx rr : ss.input_sources)
            os << rr << ",";

        os << "]" << std::endl << "\tprim-idxs: [";

        for (int idx : ss.primitives)
            os << idx << ",";

        return os << "]" << std::endl << "]";
    }
}
