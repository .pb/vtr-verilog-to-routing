#include "computation_graph.h"

#include <numeric>
#include <algorithm>

namespace fabric {
    t_cg_idx ComputationGraph::addOutputNode(t_rr_idx rr, t_pb* pb) {
        CGNode& n = addNode(pb);
        n.rr_idx = rr;
        n.depth = 0;

        outputs_.push_back(n.cg_idx);
        rr_to_cg_.emplace(rr, n.cg_idx);

        return n.cg_idx;
    }

    t_cg_idx ComputationGraph::addInputNode(t_rr_idx rr, t_pb* pb, int depth) {
        CGNode& n = addNode(pb);
        n.rr_idx = rr;
        n.depth = depth;

        inputs_.push_back(n.cg_idx);
        rr_to_cg_.emplace(rr, n.cg_idx);

        return n.cg_idx;
    }

    t_cg_idx ComputationGraph::addPrimNode(int prim_idx,
                            t_pb* pb,
                            int depth,
                            const std::vector<int>& input_spec) {
        CGNode& n = addNode(pb);
        n.prim_idx = prim_idx;
        n.depth = depth;

        prims_.push_back(n.cg_idx);
        prim_inputs_.emplace(n.cg_idx, input_spec);

        return n.cg_idx;
    }

    void ComputationGraph::addEdge(t_cg_idx source, t_cg_idx sink) {
        nodes_[source].sinks.push_back(sink);
        nodes_[sink].sources.push_back(source);
    }


    CGNode& ComputationGraph::node(t_cg_idx idx) {
        return nodes_[idx];
    }

    std::vector<t_cg_idx> ComputationGraph::sortedNodes() const {
        std::vector<t_cg_idx> idx_vec(nodes_.size());
        std::iota(idx_vec.begin(), idx_vec.end(), 0);
        std::sort(idx_vec.begin(),
                    idx_vec.end(),
                    [&] (t_cg_idx first, t_cg_idx second) -> bool {
            return nodes_[first].depth > nodes_[second].depth;
        });

        return idx_vec;
    }

    CGNode& ComputationGraph::addNode(t_pb* pb) {
        nodes_.emplace_back();
        CGNode& node = *nodes_.rbegin();
        node.pb = pb;
        node.cg_idx = nodes_.size() - 1;

        return node;
    }
}
