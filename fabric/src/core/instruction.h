#ifndef FABRIC_INSTRUCTION_H
#define FABRIC_INSTRUCTION_H

#include "computation_graph.h"
#include "vpr_types.h"

namespace fabric {
    struct instruction {
        int dest_addr;
        t_pb* prim_pb;
        t_cg_idx cg_node;
        std::vector<int> srcAddrs;
        const BitstreamPrimitive* prim;

        instruction(int destAddr,
                    t_cg_idx cg_node_val,
                    t_pb* pb,
                    const std::vector<int>& srcAddrVec):
            dest_addr(destAddr),
            prim_pb(pb),
            cg_node(cg_node_val),
            srcAddrs(srcAddrVec), prim(nullptr) {
        }

        void setPrimitive(const BitstreamPrimitive* primVal) {
            prim = primVal;
        }
    };

    std::ostream& operator<<(std::ostream& os, const instruction& i);

    typedef std::vector<instruction> InstructionList;
    typedef std::map<t_cg_idx, int> MemoryMap;
}
#endif
