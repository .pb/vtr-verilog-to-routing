#ifndef FABRIC_RR_SOURCE_LOOKUP_H
#define FABRIC_RR_SOURCE_LOOKUP_H

#include <map>
#include <vector>

#include "rr_node.h"

namespace fabric {
    typedef int t_rr_idx;

    class rr_source_lookup {
    public:
        typedef bool t_isConfigurable;
        typedef std::pair<t_rr_idx, t_isConfigurable> t_src_inf;
        typedef std::map<t_rr_idx, std::vector<t_src_inf>> t_rr_src_map;

        void insert(t_rr_idx sink, t_rr_idx source, t_isConfigurable configurable);
        const std::vector<t_src_inf> getSources(t_rr_idx sink) const;

        rr_source_lookup():
            sources() {
        };

        rr_source_lookup(t_rr_node* rr_nodes, int num_nodes);
    protected:
        t_rr_src_map sources;
    };
}

#endif
