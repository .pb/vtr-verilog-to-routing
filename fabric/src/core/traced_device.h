#ifndef FABRIC_TRACED_DEVICE_H
#define FABRIC_TRACED_DEVICE_H

#include "rr_source_lookup.h"

#include <set>
#include <list>
#include <vector>
#include <unordered_set>
#include <map>

namespace fabric {
    typedef std::unordered_set<t_rr_idx> t_rr_set;
    typedef std::list<t_rr_set> t_rr_setList;

    struct rr_super_edge {
        t_rr_idx source;
        t_rr_idx sink;

        rr_super_edge(t_rr_idx sourceVal, t_rr_idx sinkVal):
            source(sourceVal), sink(sinkVal) {
        };
    };

    struct block_super_edge {
        int source_pin;
        int sink_pin;

        block_super_edge(int source_val, int sink_val):
            source_pin(source_val), sink_pin(sink_val) {
        }
    };

    struct block_pin {
        int clb_id;
        int pin_id;

        block_pin(int clb_idVal, int pin_idVal):
            clb_id(clb_idVal), pin_id(pin_idVal) {
        }
    };

    bool operator<(const block_pin& first, const block_pin& other);

    struct primitive {
        t_pb* pb;
        int clb_id;
        int sink_pin;
        std::vector<int> source_pins;

        primitive(t_pb* pb_val,
                    int clb_id_val,
                    int sink_pin_val,
                    std::vector<int> source_pins_val):
            pb(pb_val),
            clb_id(clb_id_val),
            sink_pin(sink_pin_val),
            source_pins(source_pins_val) {
        }
    };

    typedef std::map<int, std::vector<block_super_edge>> blk_super_edge_set;

    struct source_set {
        std::vector<t_rr_idx> input_sources;
        std::vector<int> primitives;
    };

    std::ostream& operator<<(std::ostream& os, const source_set& ss);

    class TracedDevice {
    public:
        void traceRRtoRR(t_rr_idx source, t_rr_idx sink);
        void markRRasInput(t_rr_idx input, t_pb* pb);
        void markRRasOutput(t_rr_idx output, t_pb* pb);
        void markRRasOutput(const std::vector<t_rr_idx>& output,
                            const std::vector<t_pb*>& pbs);
        void nodeSets(t_rr_setList& nodeSets);
        void addBlockPintoRR(int clb_id, int block_pin, t_rr_idx rr);
        void addRRtoBlockPin(t_rr_idx rr, int clb_id, int block_pin);
        void addBlockPintoPin(int clb_id, int source, int sink);
        void addBlockPrimitive(int clb_id,
                                t_pb* pb,
                                int output,
                                const std::vector<int>& inputs);

        const std::vector<t_rr_idx>& outputs() const;
        const std::vector<t_rr_idx>& inputs() const;

        const primitive& getPrimitive(int primitive_index) const;
        t_pb* io(t_rr_idx io) const;

        source_set source_of_rr(t_rr_idx rr) const;
        source_set source_of_primitive(int primitive_index) const;

    protected:
        std::vector<rr_super_edge> rr_superedges_;
        blk_super_edge_set block_superedges_;
        std::vector<primitive> primitives_;
        std::map<t_rr_idx, t_pb*> io_pbs_;

        std::map<t_rr_idx, block_pin> rr_to_block_;
        std::map<block_pin, t_rr_idx> rr_to_block_inv_;

        std::map<block_pin, t_rr_idx> block_to_rr_;
        std::map<t_rr_idx, block_pin> block_to_rr_inv_;

        std::map<block_pin, int> primitive_by_output_pin_;
        std::vector<t_rr_idx> output_rr_;
        std::set<t_rr_idx> outsink_rr_;
        std::vector<t_rr_idx> input_rr_;

        std::map<t_rr_idx, int> rr_superedge_src_lookup_;
        std::map<block_pin, int> block_superedge_src_lookup_;

        void rr_source_subtrace(t_rr_idx rr, source_set& src) const;
    };
    //
    // Filter setList for nodes of a specific type
    std::vector<t_rr_idx> filterNodeSetList(const t_rr_setList& setList,
                                            t_rr_type rr_type);

    // Filter nodeList to remove all nodes also found in filterSet
    std::vector<t_rr_idx> filterNodeList(const std::vector<t_rr_idx>& nodeList,
                                            const t_rr_set& filterSet);
}

#endif
