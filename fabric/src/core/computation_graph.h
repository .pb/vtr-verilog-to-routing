#ifndef FABRIC_COMPUTATION_GRAPH_H
#define FABRIC_COMPUTATION_GRAPH_H

#include "rr_source_lookup.h"
#include "traced_device.h"
#include "bitstream_blocks.h"

#include <vector>
#include <map>

namespace fabric {
    struct CGNode;

    typedef int t_cg_idx;

    struct CGNode {
        std::vector<t_cg_idx> sinks;
        std::vector<t_cg_idx> sources;
        t_cg_idx cg_idx = -1;
        t_rr_idx rr_idx = -1;
        int prim_idx = -1;
        t_pb* pb = nullptr;
        int depth = -1;
    };

    class ComputationGraph {
    public:
        t_cg_idx addOutputNode(t_rr_idx rr, t_pb* pb);
        t_cg_idx addInputNode(t_rr_idx rr, t_pb* pb, int depth);
        t_cg_idx addPrimNode(int prim_idx,
                                t_pb* pb,
                                int depth,
                                const std::vector<int>& input_spec);
        void addEdge(t_cg_idx source, t_cg_idx sink);

        CGNode& node(t_cg_idx idx);
        std::vector<t_cg_idx> sortedNodes() const;
    protected:
        std::vector<CGNode> nodes_;
        std::vector<t_cg_idx> outputs_;
        std::vector<t_cg_idx> inputs_;
        std::vector<t_cg_idx> prims_;
        std::map<t_rr_idx, t_cg_idx> rr_to_cg_;
        std::map<t_cg_idx, std::vector<int>> prim_inputs_;

        CGNode& addNode(t_pb* pb);
    };

    class ExecutionGraph {
    public:
        ExecutionGraph(ComputationGraph& graph):
            cgraph_(graph),
            sorted_nodes_(graph.sortedNodes()),
            addr_map_(sorted_nodes_.size()) {

            for (int i = 0; i < sorted_nodes_.size(); i++)
                addr_map_[sorted_nodes_[i]] = i;
        }

        std::vector<t_cg_idx>& nodes() {
            return sorted_nodes_;
        }

        int lookup(t_cg_idx cg_idx) {
            return addr_map_[cg_idx];
        }

    protected:
        ComputationGraph& cgraph_;
        std::vector<t_cg_idx> sorted_nodes_;
        std::vector<int> addr_map_;
    };
}

#endif
