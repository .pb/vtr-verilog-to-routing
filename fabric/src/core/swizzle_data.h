#ifndef FABRIC_SWIZZLE_DATA_H
#define FABRIC_SWIZZLE_DATA_H

#include <map>
#include <vector>

#include "rr_source_lookup.h"

namespace fabric {
    struct rr_swizzle {
        t_rr_idx device;
        t_rr_idx block;

        rr_swizzle(t_rr_idx deviceVal, t_rr_idx blockVal):
            device(deviceVal), block(blockVal) {
        }
    };

    class SwizzleData {
    public:
        void swizzle_rr(t_rr_idx device, t_rr_idx block);
        t_rr_idx rr_block_to_device(t_rr_idx block_rr_id) const;
        t_rr_idx rr_device_to_block(t_rr_idx device_rr_id) const;

    protected:
        std::map<t_rr_idx, int> device_map_;
        std::map<t_rr_idx, int> block_map_;
        std::vector<rr_swizzle> rr_swizzles_;
    };
}

#endif
