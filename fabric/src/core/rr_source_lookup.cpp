#include "rr_source_lookup.h"

namespace fabric {
    rr_source_lookup::rr_source_lookup(t_rr_node* rr_nodes, int num_nodes) {
        for (int source_idx = 0; source_idx < num_nodes; source_idx++) {
            const t_rr_node& node = rr_nodes[source_idx];

            for (const short& edge_idx : node.non_configurable_edges())
                insert(node.edge_sink_node(edge_idx), source_idx, false);

            for (const short& edge_idx : node.configurable_edges())
                insert(node.edge_sink_node(edge_idx), source_idx, true);
        }
    }

    void rr_source_lookup::insert(t_rr_idx sink,
                                    t_rr_idx source,
                                    t_isConfigurable configurable) {
        auto it = sources.find(sink);

        if (it == sources.end()) {
            sources.emplace(make_pair(sink, std::vector<t_src_inf>{{source, configurable}}));
        } else {
            it->second.push_back(t_src_inf{source, configurable});
        }
    }

    const std::vector<rr_source_lookup::t_src_inf>
        rr_source_lookup::getSources(t_rr_idx sink) const {
        auto it = sources.find(sink);

        if (it == sources.end())
            return {};

        return it->second;
    }
}
