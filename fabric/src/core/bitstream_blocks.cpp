#include "bitstream_blocks.h"

namespace fabric {

    template <typename T>
    void BlockLookup<T>::addBlock(T&& block) {
        size_++;
        getTypeSet(block.bstmBlockType).emplace(block.index, std::move(block));
    }

    template <typename T>
    BlockTypeSet<T>& BlockLookup<T>::getTypeSet(BitstreamBlockTypeIndex typeIndex) {
        auto mapIter = map.find(typeIndex);
        if (mapIter == map.end()) {
            return map.emplace(typeIndex, BlockTypeSet<T>()).first->second;
        } else {
            return mapIter->second;
        }
    }

    template <>
    std::vector<const BitstreamTLB*> BlockLookup<BitstreamTLB>::havingDisposition(BitstreamTLBDisposition d) const {
        std::vector<const BitstreamTLB*> retVec;

        for (const auto& typeSetPair : map) {
            for (const auto& blockPair : typeSetPair.second) {
                if (blockPair.second.disposition.count(d))
                    retVec.push_back(&blockPair.second);
            }
        }

        return retVec;
    }

    template struct BlockLookup<BitstreamTLB>;
    template struct BlockLookup<BitstreamBlock>;

    std::ostream& operator<<(std::ostream& os, BitstreamTLBDisposition disp) {
        switch (disp) {
            case BitstreamTLBDisposition::INPUT:
                return os << "INPUT";

            case BitstreamTLBDisposition::OUTPUT:
                return os << "OUTPUT";

            case BitstreamTLBDisposition::CLOCK:
                return os << "CLOCK";

            case BitstreamTLBDisposition::IO:
                return os << "IO";

            case BitstreamTLBDisposition::CLB:
                return os << "CLB";

            default:
                return os << "?";
        }
    }

    std::ostream& operator<<(std::ostream& os,
                                TLBDispositionSet dispList) {
        os << "[";

        for (BitstreamTLBDisposition d : dispList)
            os << d << ",";

        return os << "]";
    }

    const BitstreamTLB* BitstreamBlocks::findTLBHavingId(BitstreamToplevelBlockId id) const {
        // FIXME: This is very bruteforce. Make this a lookup.
        for (int i = 0; i < blockTypes.numTypes(); i++) {
            for (const auto& blockPair : const_cast<BitstreamBlocks*>(this)->blocks.getTypeSet(i)) {
                if (blockPair.second.id == id)
                    return &blockPair.second;
            }
        }

        return nullptr;
    }
}
