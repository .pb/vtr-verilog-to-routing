#include "swizzle_data.h"

namespace fabric {
    void SwizzleData::swizzle_rr(t_rr_idx device, t_rr_idx block) {
        rr_swizzles_.emplace_back(device, block);
        device_map_.emplace(std::make_pair(device, rr_swizzles_.size() - 1));
        block_map_.emplace(std::make_pair(block, rr_swizzles_.size() - 1));
    }

    t_rr_idx SwizzleData::rr_block_to_device(t_rr_idx block_rr_id) const {
        if (!block_map_.count(block_rr_id))
            return block_rr_id;

        return rr_swizzles_[block_map_.find(block_rr_id)->second].device;
    }

    t_rr_idx SwizzleData::rr_device_to_block(t_rr_idx device_rr_id) const {
        if (!device_map_.count(device_rr_id))
            return device_rr_id;

        return rr_swizzles_[device_map_.find(device_rr_id)->second].block;
    }
}
