#include "fabric.h"
#include "timer.h"

#include <iostream>

namespace fabric {
    void FabricInstance::run(StageList::StageId stageId) {
        StageBase* stage = StageList::get(stageId);
        std::cout << std::endl
                    << "***[ Starting Stage: "
                    << stage->getName()
                    << " ]***********"
                    << std::endl;

        {
            TalkativeExecutionTimer t(stage->getName());
            stage->run(this);
        }
    }

    void FabricInstance::setBitstreamFilename(const std::string& s) {
        bitstreamFilename = s;
    }
}
