#ifndef FABRIC_FLOW_H
#define FABRIC_FLOW_H

#include <cstddef>
#include <string>
#include <stdexcept>
#include <iostream>

#define FABRIC_REGISTER_STAGE(name) \
    namespace fabric { \
        namespace StageImpl { \
            fabric::Stages::name name; \
        } \
        namespace Stages { \
            fabric::StageList::StageId name::id = fabric::StageList::registerStage(StageImpl::name, #name); \
        } \
    }

#define FABRIC_INIT_STAGE \
    public: \
        static fabric::StageList::StageId id;

namespace fabric {
    namespace Stage {
    }

    class FabricInstance;

    class StageBase {
    public:
        StageBase() {};
        virtual ~StageBase() noexcept(false) {};

        void init(std::string stage_name) {
            name = stage_name;
        }

        virtual void run(FabricInstance *fabric) = 0;

        std::string getName() {
            return name;
        }
    protected:
        std::string name;
        int magic;
    };

    class StageList {
        public:
            typedef size_t StageId;

            static StageId registerStage(StageBase& stage, std::string name) {
                stage.init(name);
                StageList::stages[StageList::next_id] = &stage;
                // FIXME: Add assertion to check against NUM_STAGES
                return StageList::next_id++;
            };

            static StageBase* get(StageId id) {
                if (id >= StageList::next_id)
                    throw std::runtime_error("Fail");

                return StageList::stages[id];
            }

        protected:
            static constexpr StageId NUM_STAGES = 5;
            static StageId next_id;
            static StageBase* stages[NUM_STAGES];
    };
}

#endif
