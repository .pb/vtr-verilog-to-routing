#include <sstream>
#include <fstream>

#include "bitstream.h"

#include "read_xml_arch_file.h"
#include "rr_graph2.h"
#include "vpr_utils.h"
#include "physical_types.h"
#include "globals.h"
#include "vtr_version.h"
#include "atom_netlist.h"
#include "atom_netlist_utils.h"

void vpr_bitstreamGenerator::run() {
    pugi::xml_document doc;

    std::cout << "Generating bitstream..." << std::endl;
    this->buildBitstream(doc);
    std::cout << "Writing bitstream..." << std::endl;
    this->writeBitstream(doc);
}

void vpr_bitstreamGenerator::buildBitstream(pugi::xml_document& bstmDoc) {
    std::cout << "* Header" << std::endl;
    auto& device_ctx = g_vpr_ctx.device();
    auto& cluster_ctx = g_vpr_ctx.clustering();

    pugi::xml_node bstm = bstmDoc.append_child("bitstream");

    bstm.append_attribute("format") = "1.4";
    bstm.append_attribute("tool-name") = "vpr";
    bstm.append_attribute("tool-version") = vtr::VERSION;
    bstm.append_attribute("tool-comment") = "Such final project!";

    pugi::xml_node arch_node = bstm.append_child("architecture");
    arch_node.append_attribute("origin") = get_arch_file_name();

    if (!vpr_setup.RoutingArch.write_rr_graph_filename.empty())
        arch_node.append_attribute("rr-graph") = (vpr_setup.RoutingArch.write_rr_graph_filename.c_str());

    pugi::xml_node dims = arch_node.append_child("dimension");
    dims.append_attribute("x") = std::to_string(device_ctx.grid.width()).c_str();
    dims.append_attribute("y") = std::to_string(device_ctx.grid.height()).c_str();

    pugi::xml_node channels = arch_node.append_child("channels");
    channels.append_attribute("width") = device_ctx.chan_width.max;

    pugi::xml_node layout = arch_node.append_child("layout");
    layout.append_attribute("name") = vpr_setup.device_layout.c_str();

    pugi::xml_node netlist = bstm.append_child("netlist");
    netlist.append_attribute("file") = vpr_setup.FileNameOpts.NetFile.c_str();
    netlist.append_attribute("id") = cluster_ctx.clb_nlist.netlist_id().c_str();

    std::cout << "* Placement" << std::endl;
    pugi::xml_node place = bstm.append_child("placement");
    this->getPlacement(place);

    std::cout << "* Routing" << std::endl;
    pugi::xml_node route = bstm.append_child("routing");
    this->getRouting(route);
    std::cout << "Done." << std::endl;
}

void vpr_bitstreamGenerator::getPlacement(pugi::xml_node& place) {
    auto& device_ctx = g_vpr_ctx.device();
    auto& place_ctx = g_vpr_ctx.placement();
    auto& cluster_ctx = g_vpr_ctx.clustering();
    auto& nlist = cluster_ctx.clb_nlist;
    auto& atom = g_vpr_ctx.atom();

    place.append_attribute("id") = place_ctx.placement_id.c_str();

    std::set<AtomPinId> clockset = find_netlist_logical_clock_drivers(atom.nlist);
    std::vector<AtomBlockId> atom_clk_driver_blocks;
    std::transform(clockset.begin(),
            clockset.end(),
            std::back_inserter(atom_clk_driver_blocks),
            [&](AtomPinId atom_pin) {
        return atom.nlist.pin_block(atom_pin);
    });

    std::set<ClusterBlockId> clock_driver_blocks;

    for (AtomBlockId atom_block : atom_clk_driver_blocks)
        clock_driver_blocks.insert(atom.lookup.atom_clb(atom_block));

    for (auto blk_id : nlist.blocks()) {
        std::vector<std::string> blockDispositions;
        t_block_loc location = place_ctx.block_locs[blk_id];
        t_type_ptr blk_type = nlist.block_type(blk_id);

        pugi::xml_node block = place.append_child("tlb");

        bool is_input_t = device_ctx.input_types.find(blk_type) != device_ctx.input_types.end();
        bool is_output_t = device_ctx.output_types.find(blk_type) != device_ctx.output_types.end();
        bool can_trim = true;

        if (is_input_t || is_output_t) {
            can_trim = false;
            blockDispositions.push_back("io");

            bool is_input = is_input_t && nlist.block_contains_primary_input(blk_id);
            bool is_output = is_output_t && nlist.block_contains_primary_output(blk_id);
            VTR_ASSERT(is_input || is_output);

            if (is_input) {
                blockDispositions.push_back("input");

                if (clock_driver_blocks.find(blk_id) != clock_driver_blocks.end())
                    blockDispositions.push_back("clock");
            }

            if (is_output) {
                blockDispositions.push_back("output");
            }

            block.append_attribute("z") = std::to_string(location.z).c_str();
        } else {
            blockDispositions.push_back("clb");
        }

        block.append_attribute("disposition") = string_implode(blockDispositions, ",").c_str();

        processBlock(block, nlist.block_pb(blk_id), blk_id, can_trim);

        block.append_attribute("x") = std::to_string(location.x).c_str();
        block.append_attribute("y") = std::to_string(location.y).c_str();
        block.append_attribute("name") = nlist.block_name(blk_id).c_str();
        block.append_attribute("id") = std::to_string(blk_id.data()).c_str();
    }
}

void vpr_bitstreamGenerator::processBlock(pugi::xml_node& block,
                                          t_pb *pb,
                                          ClusterBlockId block_id,
                                          bool can_trim) {
    processBlock(block, pb, pb, block_id.data(), can_trim);
}

bool vpr_bitstreamGenerator::processBlock(pugi::xml_node& block,
                                          t_pb *pb,
                                          t_pb *pb_root,
                                          int block_offset,
                                          bool can_trim = true) {
    const t_pb_type *pb_type = pb->pb_graph_node->pb_type;
    bool hasConfiguration = false;

    block.append_attribute("type") = pb_type->name;
    block.append_attribute("index") = std::to_string(pb->pb_graph_node->placement_index).c_str();
    block.append_attribute("instance") = vtr::string_fmt("%s[%d]",
                                                         pb_type->name,
                                                         block_offset).c_str();

    t_mode& mode = pb_type->modes[pb->mode];

    // Block mode configuration
    if (pb_type->num_modes > 1) {
        pugi::xml_node modenode = block.append_child("mode");
        modenode.append_attribute("width") = std::to_string(pb_type->num_modes).c_str();
        modenode.append_child(pugi::node_pcdata).set_value(hexStr(mode.index).c_str());
        hasConfiguration = true;
    }

    if (pb_type->blif_model != nullptr) {
        // How exciting! We've found a primitive...
        std::string blifModel(pb_type->blif_model);

        if (blifModel == ".names")
            hasConfiguration |= handleLUTPrimitive(block, pb);
        if (blifModel == ".latch")
            hasConfiguration |= handleFFPrimitive(block, pb);
        else if (!blifModel.compare(0, 6, ".model"))
            hasConfiguration |= handleCustomPrimitive(block, blifModel);
    }

    // Configurable Interconnect
    hasConfiguration |= processBlockInterconnects(block, pb, mode, pb_root);

    // Handle child nodes (leaf nodes have num_modes == 0)
    if (pb_type->num_modes > 0) {
        // Child nodes sorted by type
        for (int i = 0; i < mode.num_pb_type_children; i++) {
            // Offset within type
            for (int j = 0; j < mode.pb_type_children[i].num_pb; j++) {
                // If node actually valid
                if (pb->child_pbs[i] != nullptr &&
                    pb->child_pbs[i][j].name != nullptr) {
                    pugi::xml_node child = block.append_child("block");
                    hasConfiguration |= processBlock(child, &pb->child_pbs[i][j], pb_root, j);
                }
            }
        }
    }

    if (can_trim && !hasConfiguration)
        block.parent().remove_child(block);

    return hasConfiguration;
}

bool vpr_bitstreamGenerator::processBlockInterconnects(pugi::xml_node& block, t_pb *pb, t_mode& mode, t_pb *pb_root) {
    VTR_ASSERT(pb->pb_graph_node);

    if (!pb->pb_graph_node->interconnect_pins)
        return false;

    pugi::xml_node intercon_xml = block.append_child("interconnect");

    // Get t_interconnect pointers for this pb's interconnect in its current mode
    t_interconnect_pins* interconnects = pb->pb_graph_node->interconnect_pins[mode.index];
    int configuredIntercons = 0;

    for (int i = 0; i < mode.num_interconnect; i++) {
        t_interconnect_pins& intercon_pins = interconnects[i];
        pugi::xml_node intercon_spec;

        switch (intercon_pins.interconnect->type) {
            case DIRECT_INTERC:
                // Not configurable - not interested
                continue;

            case COMPLETE_INTERC:
                intercon_spec = intercon_xml.append_child("complete");
                break;

            case MUX_INTERC:
                intercon_spec = intercon_xml.append_child("mux");
                break;

            default:
                VPR_THROW(VPR_ERROR_BITSTREAM,
                        "Unknown interconnect type %d",
                        intercon_pins.interconnect->type);
        }

        intercon_spec.append_attribute("name") = intercon_pins.interconnect->name;
        intercon_spec.append_attribute("width") =
            std::to_string(intercon_pins.interconnect->interconnect_power->num_input_ports).c_str();

        configuredIntercons++;
        getInterconnectConfiguration(intercon_pins, pb_root, intercon_spec);
    }

    if (!configuredIntercons)
        intercon_xml.parent().remove_child(intercon_xml);

    return configuredIntercons;
}

void vpr_bitstreamGenerator::getInterconnectConfiguration(t_interconnect_pins& intercon_pins,
                                                            t_pb *pb_root,
                                                            pugi::xml_node& interconSpec) {
    int num_inports = intercon_pins.interconnect->interconnect_power->num_input_ports;
    int num_outports = intercon_pins.interconnect->interconnect_power->num_output_ports;
    int num_portpins = intercon_pins.interconnect->interconnect_power->num_pins_per_port;

    VTR_ASSERT(pb_root->pb_route);

    for (int outport_idx = 0; outport_idx < num_outports; outport_idx++) {
        int inport_idx = 0;

        for (; inport_idx < num_inports; inport_idx++) {
            for (int pin_idx = 0; pin_idx < num_portpins; pin_idx++) {
            t_pb_graph_pin *output_pin = intercon_pins.output_pins[outport_idx][pin_idx];
            t_pb_graph_pin *input_pin = intercon_pins.input_pins[inport_idx][pin_idx];

            if (!output_pin || !input_pin) {
                continue;
            }

            VTR_ASSERT(input_pin && output_pin);

            t_pb_route& routeEdge = pb_root->pb_route[output_pin->pin_count_in_cluster];

            if (routeEdge.driver_pb_pin_id != OPEN &&
                routeEdge.driver_pb_pin_id == input_pin->pin_count_in_cluster)
                goto found_route;
            }
        }

        continue; // Ignore outport if no route found
found_route:
        pugi::xml_node out = interconSpec.append_child("out");
        out.append_attribute("id") = std::to_string(outport_idx).c_str();
        out.append_child(pugi::node_pcdata).set_value(hexStr(inport_idx).c_str());
    }
}

bool vpr_bitstreamGenerator::handleLUTPrimitive(pugi::xml_node& block, t_pb *pb) {
    auto& atom_ctx = g_vpr_ctx.atom();
    const t_pb_type *pb_type = pb->pb_graph_node->pb_type;
    int lut_width = pb_type->num_input_pins;
    AtomBlockId atom_block = atom_ctx.lookup.pb_atom(pb);
    const AtomNetlist::TruthTable& ttable = atom_ctx.nlist.block_truth_table(atom_block);
    const AtomNetlist::TruthTable& exp_ttable = expand_truth_table(ttable, lut_width);
    std::vector<vtr::LogicValue> lut_mask = truth_table_to_lut_mask(exp_ttable, lut_width);

    pugi::xml_node names = block.append_child("names");
    names.append_attribute("width") = std::to_string(lut_width).c_str();
    names.append_child(pugi::node_pcdata).set_value(packLUTMask(lut_mask).c_str());

    return true;
}

bool vpr_bitstreamGenerator::handleFFPrimitive(pugi::xml_node& block, t_pb *pb) {
    auto& atom_ctx = g_vpr_ctx.atom();
    AtomBlockId atom_block = atom_ctx.lookup.pb_atom(pb);
    const AtomNetlist::TruthTable& ttable = atom_ctx.nlist.block_truth_table(atom_block);
    vtr::LogicValue initial = ttable[0][0];

    pugi::xml_node ff = block.append_child("ff");
    pugi::xml_node initial_n = ff.append_child("init");
    std::stringstream ss;

    ss << "0x" << initial;

    initial_n.append_child(pugi::node_pcdata).set_value(ss.str().c_str());

    return true;
}

bool vpr_bitstreamGenerator::handleCustomPrimitive(pugi::xml_node& block,
                                                    std::string blifModel) {
    pugi::xml_node model = block.append_child("subckt");
    std::string modelName = blifModel.substr((std::string(".subckt ").length()));
    model.append_attribute("model") = modelName.c_str();
    // TODO: subckt attributes from the blif should probably end up in the bitstream!
    return false;
}

std::string vpr_bitstreamGenerator::hexStr(int n) {
    std::ostringstream os;
    os << "0x" << std::hex << n;
    return os.str();
}

std::string vpr_bitstreamGenerator::packLUTMask(std::vector<vtr::LogicValue> lut_mask) {
    std::stringstream mask_str("0x");
    int nibble = 0, i = 0;

    mask_str << "0x" << std::hex;

    for (vtr::LogicValue value : lut_mask) {
        if (i == 4) {
            i = 0;
            mask_str << nibble;
            nibble = 0;
        }

        nibble += (1 << i) * (value == vtr::LogicValue::TRUE);
        i++;
    }

    mask_str << nibble;

    return mask_str.str();
}

void vpr_bitstreamGenerator::getRouting(pugi::xml_node& route) {
    auto& route_ctx = g_vpr_ctx.routing();
    route.append_attribute("id") = route_ctx.routing_id.c_str();

    std::vector<t_rr_switchbit_id> switchbits = getUsedSwitchbits();
    std::vector<std::string> sbitstring;
    std::transform(switchbits.begin(),
                    switchbits.end(),
                    std::back_inserter(sbitstring),
                    [](t_rr_switchbit_id id) {
                        return std::to_string((int) id);
                    });
    route.append_child(pugi::node_pcdata).set_value(string_implode(sbitstring, ",").c_str());
}

std::vector<t_rr_switchbit_id> vpr_bitstreamGenerator::getUsedSwitchbits() {
    auto& cluster_ctx = g_vpr_ctx.clustering();
    auto& route_ctx = g_vpr_ctx.routing();
    auto& device_ctx = g_vpr_ctx.device();

    t_node_id current_node = -1, last_node;
    std::vector<t_rr_switchbit_id> used_sbits;

    for (auto net_id : cluster_ctx.clb_nlist.nets()) {
        // Ignore global nets
        if (cluster_ctx.clb_nlist.net_is_global(net_id))
            continue;

        // Ignore nets that have no sinks
        if (cluster_ctx.clb_nlist.net_sinks(net_id).size() == false)
            continue;

        for (t_trace *trace = route_ctx.trace_head[net_id];
                trace != nullptr;
                trace = trace->next) {
            last_node = current_node;
            current_node = trace->index;

            t_rr_type rr_type = device_ctx.rr_nodes[current_node].type();

            switch (rr_type) {
            case SOURCE: // Source always starts the trace. Find the next node.
                continue;

            case OPIN: // Even though Source -> OPIN is a virtual edge, generate a switchbit.
            case IPIN:
            case CHANX:
            case CHANY:
            case SINK:
                {
                    // Is there a switchbit between the last node and this node?
                    t_rr_switchbit_id_map bits = device_ctx.rr_switchbits.get(last_node, current_node);
                    if (bits.size())
                        used_sbits.push_back(bits.begin()->first);
                }

                break;

            default:
                VPR_THROW(VPR_ERROR_BITSTREAM,
                            "Unknown trace element type for node %d: %d (%s)",
                            current_node,
                            rr_type,
                            device_ctx.rr_nodes[current_node].type_string());
                continue;
            }

            if (rr_type == SINK && trace->next != nullptr) {
                /*
                 * The node after a SINK, if present, is the branchpoint in a
                 * multi-SINK net (which for a valid routing, we will have already
                 * seen). Advance the current node to the branchpoint so that we
                 * start tracing the next branch on the next for-loop iteration.
                 */
                current_node = trace->next->index;
                trace = trace->next;
            }
        }
    }

    return used_sbits;
}

void vpr_bitstreamGenerator::writeBitstream(pugi::xml_document& bstmDoc) {
    std::ofstream bitstreamFile;
    bitstreamFile.open(vpr_setup.FileNameOpts.BitstreamFile);

    if (!bitstreamFile)
        VPR_THROW(VPR_ERROR_BITSTREAM, "Failed to open bitstream file for writing!");

    bstmDoc.save(bitstreamFile);
}
