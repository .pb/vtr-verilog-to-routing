#ifndef VPR_BITSTREAM_H
#define VPR_BITSTREAM_H

#include "vpr_types.h"
#include "arch_types.h"
#include "rr_graph2.h"
#include "vtr_logic.h"
#include "clustered_netlist_fwd.h"

#include "pugixml.hpp"
#include "pugixml_util.hpp"

class vpr_bitstreamGenerator {
public:
    vpr_bitstreamGenerator(t_vpr_setup& vpr_setup_inf, const t_arch& arch_inf):
        vpr_setup(vpr_setup_inf),
        arch(arch_inf) {
    };

    void run();

protected:
    const t_vpr_setup& vpr_setup;
    const t_arch& arch;

    void buildBitstream(pugi::xml_document& bstm);
    void writeBitstream(pugi::xml_document& bstm);

    void getPlacement(pugi::xml_node& place);
    void getRouting(pugi::xml_node& route);
    std::vector<t_rr_switchbit_id> getUsedSwitchbits();

    void processBlock(pugi::xml_node& block, t_pb *pb, ClusterBlockId block_id, bool can_trim);
    bool processBlock(pugi::xml_node& block, t_pb *pb, t_pb *pb_root, int block_offset, bool can_trim);

    bool processBlockInterconnects(pugi::xml_node& block, t_pb *pb, t_mode& mode, t_pb *pb_root);
    void getInterconnectConfiguration(t_interconnect_pins& intercon_pins, t_pb *pb_root, pugi::xml_node& interconSpec);

    bool handleLUTPrimitive(pugi::xml_node& block, t_pb *pb);
    bool handleFFPrimitive(pugi::xml_node& block, t_pb *pb);
    bool handleCustomPrimitive(pugi::xml_node& block, std::string blifModel);

    std::string hexStr(int n);
    std::string packLUTMask(std::vector<vtr::LogicValue> lut_mask);
};

#endif
