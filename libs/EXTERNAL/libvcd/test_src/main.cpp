#include <cstdio>
#include <cstring>
#include <cerrno>
#include <iostream>
#include <string>

#include "libvcd_lexer.h"
#include "vcd.tab.hh"

int main(int argc, const char** argv) {
    if (argc < 2)
        return 1;

    FILE* f = fopen(argv[1], "r");

    if (!f) {
        std::cout << strerror(errno) << std::endl;
        return 2;
    }

    libvcd::vcd vcd;
    libvcd::vcd_tokwriter writer(vcd);

    libvcd::lexer lex;
    lex.setSourceFile(f);

    libvcd::parser p(lex, writer);
    p.set_debug_level(10);

    if (p.parse()) {
        std::cout << "Parsing failed." << std::endl;
        return 3;
    }

    std::cout << "Parse success." << std::endl << std::endl;
    std::cout << "Readback:" << std::endl << vcd;

    return 0;
}
