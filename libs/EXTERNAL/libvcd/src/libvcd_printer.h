#ifndef LIBVCD_PRINTER_H
#define LIBVCD_PRINTER_H

#include "libvcd_vcd.h"
#include "libvcd_reader.h"

namespace libvcd {
    class vcd_printer {
    public:
        vcd_printer(const vcd& v, vcd_reader& r);
        std::ostream& operator()(std::ostream& os);
        void printDate(std::ostream& os);
        void printVersion(std::ostream& os);
        void printFileComment(std::ostream& os);
        void printTimescale(std::ostream& os);
        void printVars(std::ostream& os);
        void printVars(std::ostream&, const scope* s, bool printThis = true);
        void printReader(std::ostream& os);
        void printNet(std::ostream& os, const net* n);
        void printTime(std::ostream& os, time_t time);
        void printComments(std::ostream& os, const std::vector<std::string>& comments);

    protected:
        const vcd& v_;
        vcd_reader& r_;

        void printBlock(std::ostream& os,
                        const std::string& name,
                        const std::string& contents = "");
    };
}

#endif
