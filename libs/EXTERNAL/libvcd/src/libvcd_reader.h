#ifndef LIBVCD_READER_H
#define LIBVCD_READER_H

#include "libvcd_vcd.h"

namespace libvcd {
    class vcd_printer;

    class vcd_reader {
        public:
            vcd_reader(const vcd& vcdump);

            const vcd& get() const;

            time_t  time();
            bool    seek(time_t to);
            bool    seekNext();
            void    rewind();

            const VVal&     scalar(net::id_t net) const;
            const VVec&     vector(net::id_t net) const;
            const float&    real(net::id_t net) const;

            time_t endsAt() const;

            dump_type dumpType() const;
            const std::vector<std::string> comments() const;

            friend class vcd_printer;
        protected:
            const vcd& vcd_;
            int seek_idx_;
            std::vector<VVal> state_scalar_;
            std::vector<VVec> state_vector_;
            std::vector<float> state_real_;

            void initStateVectors();
    };
}

#endif
