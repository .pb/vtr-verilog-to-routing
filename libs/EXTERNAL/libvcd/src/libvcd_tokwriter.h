#ifndef LIBVCD_TOKWRITER_H
#define LIBVCD_TOKWRITER_H

#include "libvcd_writer.h"
#include "libvcd_tokens.h"
#include "libvcd_types.h"

#include<string>
#include<sstream>

namespace libvcd {
    class vcd_tokwriter : public vcd_writer {
    public:
        vcd_tokwriter(vcd& vcdump):
            vcd_writer(vcdump) {
        }

        void date(const std::string& text);
        void timescale(tok::Decimal& timeVal, const std::string& unit);
        void var(var_type typ,
                    tok::Decimal& size,
                    const std::string& netId,
                    const std::string& ident,
                    const tok::index& idx);
        void change(const tok::value_change& change);
        void sim_time(const std::string& timeStr);
        void dump(const tok::dump& dump_tok);
        void comment_start();
        void comment_text(const std::string& text);
        void comment_end();
    protected:
        std::stringstream comment_buffer_;
    };
}

#endif
