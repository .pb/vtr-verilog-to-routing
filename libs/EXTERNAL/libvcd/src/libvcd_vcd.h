#ifndef LIBVCD_VCD_H
#define LIBVCD_VCD_H

#include <string>
#include <vector>
#include <map>

#include "libvcd_types.h"
#include "libvcd_vval.h"

#define LIBVCD_PTR_LISTS 1

namespace libvcd {
    class scope;

    class net {
    public:
        typedef std::string ident_t;
        typedef int id_t;

        net(const std::string& name,
            var_type varType,
            unsigned char vec_width,
            index_t idx,
            const scope& scope);

        const scope& net_scope() const;
        id_t id() const;
        const ident_t& ident() const;
        const std::string& name() const;
        net_type netType() const;
        var_type varType() const;
        unsigned char vec_width() const;
        void setId(id_t id);
        void setIdent(const ident_t& ident);

        const id_t INVALID = -1;
    protected:
        std::string name_;
        std::string full_name_;
        id_t id_;
        ident_t ident_;
        net_type type_;
        var_type vartype_;
        unsigned char vec_width_;
        index_t index_;
        const scope& scope_;
    };

    class netmap;

    class scope {
    public:
        scope(scope* parent,
                scope_type type,
                const std::string& name);

        scope& addChild(scope_type type,
                        const std::string& name);

        void addNet(net::id_t net);

        scope* childWithName(const std::string& name);
        net* netWithName(const std::string& name, const netmap& map);

        const std::string& name() const;
        std::vector<const scope*> children() const;
        const std::vector<net::id_t>& nets() const;
        scope_type type() const;
        scope* parent();
    protected:
        const std::string name_;
        scope_type type_;
        scope* parent_;
        std::vector<net::id_t> nets_;
        std::vector<scope> children_;
    };

    class netmap {
    public:
        net::id_t addNet(net net_ref);
        net::id_t addMap(net net_ref, const net::ident_t& ident);

        const net* getNet(const net::ident_t& ident) const;
        net* getNet(const net::id_t& id) const;

        const std::vector<net>& getNets() const;

        const size_t& scalarCount() const;
        const size_t& vectorCount() const;
        const size_t& realCount() const;

        const size_t& getTypeIndex(const net::id_t& id) const;

    protected:
        const net::ident_t generateIdent();
        unsigned int ident_idx_ = 0;
        std::map<const net::ident_t, const net::id_t> ident_map_;
        std::vector<size_t> type_index_lookup_;
        std::vector<net> nets_;

        size_t scalar_count_    = 0;
        size_t vector_count_    = 0;
        size_t real_count_      = 0;
    };

    typedef int time_t;

    struct vcd_entry_data {
    protected:
        typedef std::vector<std::pair<net::id_t, VVal>> vec_scalar;
        typedef std::vector<std::pair<net::id_t, VVec>> vec_vector;
        typedef std::vector<std::pair<net::id_t, float>> vec_real;

    public:
        vec_scalar& scalars() {
#if LIBVCD_PTR_LISTS
            if (scalars_ == nullptr)
                scalars_ = new vec_scalar;

            return *scalars_;
#else
            return scalars_;
#endif
        }

        vec_vector& vectors() {
#if LIBVCD_PTR_LISTS
            if (vectors_ == nullptr)
                vectors_ = new vec_vector;

            return *vectors_;
#else
            return vectors_;
#endif
        }

        vec_real& reals() {
#if LIBVCD_PTR_LISTS
            if (reals_ == nullptr)
                reals_ = new vec_real;

            return *reals_;
#else
            return reals_;
#endif
        }

#if LIBVCD_PTR_LISTS
        ~vcd_entry_data() {
            if (scalars_ != nullptr) {
                delete scalars_;
                scalars_ = nullptr;
            }

            if (vectors_ != nullptr) {
                delete vectors_;
                vectors_ = nullptr;
            }

            if (reals_ != nullptr) {
                delete reals_;
                reals_ = nullptr;
            }
        }

        vcd_entry_data& operator=(vcd_entry_data& other) {
            std::swap(scalars_, other.scalars_);
            std::swap(vectors_, other.vectors_);
            std::swap(reals_, other.reals_);

            return *this;
        }
        vcd_entry_data(vcd_entry_data& other) = delete;

        vcd_entry_data(vcd_entry_data&& other) {
            std::swap(scalars_, other.scalars_);
            std::swap(vectors_, other.vectors_);
            std::swap(reals_, other.reals_);
        }

        vcd_entry_data():
            scalars_(nullptr), vectors_(nullptr), reals_(nullptr) {
        }
#endif

    protected:
#if LIBVCD_PTR_LISTS
        vec_scalar*  scalars_ = nullptr;
        vec_vector* vectors_ = nullptr;
        vec_real*   reals_ = nullptr;
#else
        vec_scalar  scalars_;
        vec_vector vectors_;
        vec_real   reals_;
#endif
    };

    struct vcd_entry_indexes {
        int real_idx = -1;
        int vector_idx = -1;
        int scalar_idx = -1;

        int real_count = 0;
        int vector_count = 0;
        int scalar_count = 0;

        void scalar(int index) {
            if (scalar_idx < 0)
                scalar_idx = index;

            scalar_count++;

            //TODO: AAHHH ADD CONTIGUOUS ASSERTION OR STH
        };

        void vector(int index) {
            if (vector_idx < 0)
                vector_idx = index;

            vector_count++;

            //TODO: AAHHH ADD CONTIGUOUS ASSERTION OR STH
        };

        void real(int index) {
            if (real_idx < 0)
                real_idx = index;

            real_count++;

            //TODO: AAHHH ADD CONTIGUOUS ASSERTION OR STH
        };
    };

    template <typename T>
    std::pair<typename std::vector<T>::iterator,
                typename std::vector<T>::iterator>
        getIndexPair(std::vector<T>& v,
                        typename std::vector<T>::size_type start_idx,
                        typename std::vector<T>::size_type count) {
        auto startIt = v.begin();
        auto endIt = v.begin();

        std::advance(startIt, start_idx);
        std::advance(endIt, start_idx + count); // 1 past end

        return std::make_pair(startIt, endIt);
    }

    class vcd_entry {
    public:
        vcd_entry(time_t time, dump_type type);
        time_t time() const;
        dump_type type() const;
        void setType(dump_type t);
        vcd_entry_indexes& indexes() {
            return indexes_;
        }

        const vcd_entry_indexes& const_indexes() const {
            return indexes_;
        }

    protected:
        time_t time_;
        dump_type type_;
        vcd_entry_indexes indexes_;
    };

    class vcd_reader;
    class vcd_writer;
    class vcd_printer;

    class vcd {
    public:
        vcd();

        const std::string& version() const;
        const std::string& fileComment() const;
        const std::string& date() const;

        net* lookupNet(const std::string& netName);
        const net* getNet(net::id_t id);
        std::vector<std::pair<net::id_t, std::string>> getNets();

        vcd_entry_data& data();
        const vcd_entry_data& const_data() const;

        friend class vcd_reader;
        friend class vcd_writer;
        friend class vcd_tokwriter;
        friend class vcd_printer;
    protected:
        std::string version_;
        std::string file_comment_;
        std::string file_date_;
        timescale timescale_;
        netmap map_;
        scope root_;
        std::vector<vcd_entry> entries_;
        std::vector<std::string> comments_;
        vcd_entry_data data_;

        // Map from entries_[idx] -> vector<comments_[idx]>
        std::map<int, std::vector<int>> comment_indexes_;
    };

    const std::vector<std::string> explode_hierarchical(const std::string& str);

    std::ostream& operator<<(std::ostream& os, const vcd& vcd);
}

#endif
