#ifndef H_LIBVCD_VVAL_H
#define H_LIBVCD_VVAL_H

#include <iostream>
#include <vector>

namespace libvcd {
    class IndeterminateComparison : std::runtime_error {
    public:
        IndeterminateComparison():
            std::runtime_error("An indeterminate boolean comparison occurred.") {
        }
    };

    class IndeterminateCast : std::runtime_error {
    public:
        IndeterminateCast():
            std::runtime_error("An indeterminate numerical cast occurred.") {
        }
    };

    struct VVal {
        typedef char val_t;
        static constexpr const val_t ZERO = 0;
        static constexpr const val_t ONE  = 1;
        static constexpr const val_t X    = 2;
        static constexpr const val_t Z    = 3;

        constexpr VVal():
            val_(VVal::X) {
        }

        constexpr VVal(val_t value):
            val_(value) {
        }

        constexpr VVal(const char* strRep):
            val_(VVal::lookupChar(strRep)) {
        };

        constexpr static val_t lookupChar(const char* ch) {
            switch (*ch) {
                case '0':
                    return ZERO;

                case '1':
                    return ONE;

                case 'Z':
                    return Z;

                case 'X':
                default:
                    return X;
            }
        }

        constexpr static val_t lookupChar(char ch) {
            return lookupChar(&ch);
        }

        VVal(const std::string& str) {
            if (!str.size())
                val_ = X;

            val_ = VVal::lookupChar(&str[0]);
        }

        explicit operator bool() const {
            if (!driven())
                throw IndeterminateComparison();

            return val_ > 0;
        };

        operator val_t() const {
            return val_;
        }

        constexpr bool operator==(const VVal& cmp) const {
            if (!cmp.driven() || !driven())
                return false;

            return val_ == cmp.val_;
        };

        constexpr bool operator==(bool cmp) const {
            if (!driven())
                return false;

            return (cmp && val_ > 0) ||
                    (!cmp && val_ == 0);
        };

        constexpr bool operator!=(bool cmp) const {
            if (!driven())
                return false;

            return (!cmp && val_ > 0) ||
                    (cmp && val_ == 0);
        };

        constexpr bool operator>(const VVal& cmp) const {
            if (!driven())
                return false;

            return (val_ > 0) && (cmp.val_ == 0);
        };

        constexpr bool operator<(const VVal& cmp) const {
            if (!driven())
                return false;

            return (cmp.val_ > 0) && (val_ == 0);
        };

        constexpr VVal operator+(const VVal& cmp) const {
            if (val_ == X || cmp.val_ == X)
                return X;

            if (val_ == ONE || cmp.val_ == ONE)
                return ONE;

            if (val_ == ZERO || cmp.val_ == ZERO )
                return ZERO;

            if ((val_ == ONE && cmp.val_ == Z)
                    || (val_ == Z && cmp.val_ == ONE)) {
                return ONE;
            }

            if ((val_ == ZERO && cmp.val_ == Z)
                    || (val_ == Z && cmp.val_ == ZERO)) {
                return ZERO;
            }

            if (val_ == Z && cmp.val_ == Z)
                return Z;

            return X;
        };

        constexpr VVal operator+=(const VVal& cmp) const {
            return *this + cmp;
        };

        constexpr VVal operator^(const VVal& cmp) const {
            if (!driven() || !cmp.driven())
                return X;

            return (val_ == ONE && cmp.val_ == ZERO) ||
                    (val_ == ZERO && cmp.val_ == ONE);
        };

        constexpr VVal operator&(const VVal& cmp) const {
            if (!driven() || !cmp.driven())
                return X;

            return val_ == ONE && cmp.val_ == ONE;
        };

        constexpr VVal operator&&(const VVal& cmp) const {
            if (!driven() || !cmp.driven())
                return X;

            return val_ == ONE && cmp.val_ == ONE;
        };

        constexpr VVal operator|(const VVal& cmp) const {
            if (!driven() || !cmp.driven())
                return X;

            return val_ == ONE || cmp.val_ == ONE;
        };

        constexpr VVal operator||(const VVal& cmp) const {
            if (!driven() || !cmp.driven())
                return X;

            return val_ == ONE || cmp.val_ == ONE;
        };

        constexpr VVal operator~() const {
            if (!driven())
                return X;

            return !val_;
        };

        constexpr VVal operator!() const {
            if (!driven())
                return X;

            return !val_;
        };

        constexpr bool driven() const {
            return !(val_ & 2);
        };

    private:
        friend std::ostream& operator<<(std::ostream&, const VVal&);
        val_t val_;
    };

    struct VVec {
        VVec():
            width_(0), vec_() {
        };

        VVec(unsigned char width);
        VVec(const VVal& val, unsigned char width);
        VVec(const std::vector<VVal>& vals);
        VVec(const std::vector<VVal>& vals, unsigned char width);
        VVec(const std::string&, unsigned char width);
        void extend(unsigned char width);
        void truncate(unsigned char width);
        void resize(unsigned char width);
        unsigned char width() const;
        std::vector<VVal>& expand_inplace();
        std::vector<VVal> expand_copy() const;

        const VVal operator[](unsigned char index) const;
        VVal& at(unsigned char index);

    protected:
        friend std::ostream& operator<<(std::ostream&, const VVec&);
        unsigned char width_;
        std::vector<VVal> vec_;

        const VVal defaultval() const;
    };
}

#endif
