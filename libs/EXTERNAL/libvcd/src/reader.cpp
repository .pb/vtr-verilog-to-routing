#include "libvcd_reader.h"

#include <cmath>

namespace libvcd {
    vcd_reader::vcd_reader(const vcd& vcdump):
        vcd_(vcdump),
        seek_idx_(-1),
        state_scalar_(vcdump.map_.scalarCount()),
        state_vector_(vcdump.map_.vectorCount()),
        state_real_(vcdump.map_.realCount(), nanf(""))
    {
    }

    const vcd& vcd_reader::get() const {
        return vcd_;
    }

    time_t vcd_reader::time() {
        if (seek_idx_ < 0)
            return 0;

        return vcd_.entries_[seek_idx_].time();
    }

    time_t vcd_reader::endsAt() const {
        if (!vcd_.entries_.size())
            return 0;

        return vcd_.entries_[vcd_.entries_.size() - 1].time();
    }

    bool vcd_reader::seek(time_t to) {
        if (to < time())
            return false;

        if (seek_idx_ > 0 && to == time())
            return true;

        while (seekNext()) {
            if (time() >= to)
                return true;
        }

        return false;
    }

    bool vcd_reader::seekNext() {
        if (((signed int) seek_idx_) + 1 >= (signed int) vcd_.entries_.size())
            return false;

        seek_idx_++;

        vcd_entry_data& d = const_cast<vcd_entry_data&>(vcd_.const_data());
        const vcd_entry_indexes& idx = vcd_.entries_[seek_idx_].const_indexes();

        if (vcd_.map_.scalarCount()) { // Prevent construction if not exists
            auto idx_pair = getIndexPair(d.scalars(), idx.scalar_idx, idx.scalar_count);

            for (auto v = idx_pair.first; v != idx_pair.second; v++)
                state_scalar_[vcd_.map_.getTypeIndex(v->first)] = v->second;
        }

        if (vcd_.map_.vectorCount()) { // Prevent construction if not exists
            auto idx_pair = getIndexPair(d.vectors(), idx.vector_idx, idx.vector_count);

            for (auto v = idx_pair.first; v != idx_pair.second; v++)
                state_vector_[vcd_.map_.getTypeIndex(v->first)] = v->second;
        }

        if (vcd_.map_.realCount()) { // Prevent construction if not exists
            auto idx_pair = getIndexPair(d.reals(), idx.real_idx, idx.real_count);

            for (auto v = idx_pair.first; v != idx_pair.second; v++)
                state_real_[vcd_.map_.getTypeIndex(v->first)] = v->second;
        }

        return true;
    }

    void vcd_reader::rewind() {
        seek_idx_ = -1;
        initStateVectors();
    }

    const VVal& vcd_reader::scalar(net::id_t net) const {
        return state_scalar_[vcd_.map_.getTypeIndex(net)];
    }

    const VVec& vcd_reader::vector(net::id_t net) const {
        return state_vector_[vcd_.map_.getTypeIndex(net)];
    }

    const float& vcd_reader::real(net::id_t net) const {
        return state_real_[vcd_.map_.getTypeIndex(net)];
    }

    dump_type vcd_reader::dumpType() const {
        if (seek_idx_ < 0)
            return dump_type::NONE;

        return vcd_.entries_[seek_idx_].type();
    }

    const std::vector<std::string> vcd_reader::comments() const {
        std::vector<std::string> retvec;

        auto idx_entry_it = vcd_.comment_indexes_.find(seek_idx_);
        if (idx_entry_it == vcd_.comment_indexes_.end())
            return retvec;

        for (auto it = idx_entry_it->second.begin();
                it != idx_entry_it->second.end();
                it++) {
            retvec.push_back(vcd_.comments_[*it]);
        }

        return retvec;
    }

    void vcd_reader::initStateVectors() {
        state_scalar_.clear();
        state_vector_.clear();
        state_real_.clear();

        state_scalar_.resize(vcd_.map_.scalarCount()),
        state_vector_.resize(vcd_.map_.vectorCount()),
        state_real_.resize(vcd_.map_.realCount(), nanf(""));
    }
}
