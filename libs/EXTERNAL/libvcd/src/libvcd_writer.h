#ifndef LIBVCD_WRITER_H
#define LIBVCD_WRITER_H

#include "libvcd_vcd.h"
#include "libvcd_types.h"

namespace libvcd {
    class vcd_writer {
    public:
        vcd_writer(vcd& vcdump);

        void comment(const std::string& comment);
        void time(time_t t);
        time_t time() const;
        void setCurrentType(dump_type t);

        void enterScope(scope_type sc_type, const std::string& ident);
        void upscope();

        void endDefs();

        void setVersion(const std::string& str);
        void setFileComment(const std::string& comment);
        void setDate(const std::string& date);
        void setDate();
        void setTimescale(int value, time_unit unit);

        vcd& get();

        void scalar(net::id_t net, const VVal& val);
        void vector(net::id_t net, const VVec& val);
        void real(net::id_t net, float val);

        // FIXME: Add methods for declaring other types of variable
        net::id_t declScalar(var_type typ,
                               const std::string& netName);
    protected:
        bool writing_definitions_;
        vcd& vcd_;
        int  seek_idx_;
        scope* current_scope_;
    };
}

#endif
