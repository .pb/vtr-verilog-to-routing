#include "libvcd_vcd.h"
#include "libvcd_version.h"

#include <sstream>

namespace libvcd {
    vcd_entry::vcd_entry(time_t time, dump_type type):
        time_(time), type_(type) {
    }

    time_t vcd_entry::time() const {
        return time_;
    }

    dump_type vcd_entry::type() const {
        return type_;
    }

    vcd_entry_data& vcd::data() {
        return data_;
    }

    const vcd_entry_data& vcd::const_data() const {
        return data_;
    }

    void vcd_entry::setType(dump_type t) {
        type_ = t;
    }

    vcd::vcd():
        version_(),
        file_comment_(),
        file_date_(),
        timescale_(),
        map_(),
        root_(scope(nullptr, scope_type::ROOT, "")),
        data_() {
    }

    const std::string& vcd::version() const {
        return version_;
    }

    const std::string& vcd::fileComment() const {
        return file_comment_;
    }

    const std::string& vcd::date() const {
        return file_date_;
    }

    net* vcd::lookupNet(const std::string& netName) {
        const std::vector<std::string> hier = explode_hierarchical(netName);
        scope* scp = &root_;

        for (int i = 0; i < hier.size() - 1; i++) {
            if (!(scp = scp->childWithName(hier[i])))
                return nullptr;
        }

        return scp->netWithName(*hier.rbegin(), map_);
    }

    const net* vcd::getNet(net::id_t id) {
        return map_.getNet(id);
    }

    std::vector<std::pair<net::id_t, std::string>> vcd::getNets() {
        std::vector<std::pair<net::id_t, std::string>> retvec;

        for (const net& n : map_.getNets())
            retvec.emplace_back(n.id(), n.name());

        return retvec;
    }

    const std::vector<std::string> explode_hierarchical(const std::string& str) {
        std::vector<std::string> retvec;

        /* FIXME: This is woefully naieve because escaped identifiers, however
         * I'm fresh out of expletives to distribute right now. */
        for (std::string tok; std::getline(std::stringstream(str), tok, '.'); )
            retvec.emplace_back(tok);

        return retvec;
    }
}
