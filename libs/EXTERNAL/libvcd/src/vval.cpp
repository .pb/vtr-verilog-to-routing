#include "libvcd_vval.h"

#include <algorithm>

namespace libvcd {
    std::ostream& operator<<(std::ostream& os, const VVal& v) {
        switch (v.val_) {
            case VVal::ZERO:
                return os << "0";

            case VVal::ONE:
                return os << "1";

            case VVal::Z:
                return os << "Z";

            case VVal::X:
            default:
                return os << "X";
        }
    }

    std::ostream& operator<<(std::ostream& os, const VVec& val) {
        os << "b";

        size_t w_diff = val.width_ - val.vec_.size();

        VVal def;

        if (w_diff)
            def = val.defaultval();

        while (w_diff--)
            os << def;

        for (auto it = val.vec_.rbegin(); it != val.vec_.rend(); it++)
            os << *it;

        return os;
    }

    VVec::VVec(unsigned char width):
        width_(width), vec_() {
    }

    VVec::VVec(const VVal& val, unsigned char width):
        width_(width), vec_(width, val) {
    }

    VVec::VVec(const std::vector<VVal>& vals):
        width_(vals.size()), vec_(vals) {
    }

    VVec::VVec(const std::vector<VVal>& vals, unsigned char width):
        width_(width),
        vec_(vals) {
        // Truncate data in excess of vector stated width
        if ((std::vector<VVal>::size_type) width < vec_.size())
            vec_.resize(width);
    }

    VVec::VVec(const std::string& str, unsigned char width):
        width_(width),
        vec_(std::min(str.size() - 1, (std::vector<VVal>::size_type) width)) {
        // Str size is too big by 1 as it has leading b

        auto strIt = str.rbegin();

        for (auto vecIt = vec_.begin(); vecIt != vec_.end(); vecIt++, strIt++) {
            *vecIt = VVal::lookupChar(*strIt);
        }
    }

    void VVec::extend(unsigned char width) {
        if (width < width_)
            throw std::invalid_argument("VVec requestd extend() to smaller width");

        width_ = width;
    }

    void VVec::truncate(unsigned char width) {
        if (width > width_)
            throw std::invalid_argument("VVec requestd truncate() to larger width");

        if (width < width_) {
            vec_.resize(width);
            width_ = width;
        }
    }

    void VVec::resize(unsigned char width) {
        if (width > width_)
            width_ = width;

        if (width < width_) {
            vec_.resize(width);
            width_ = width;
        }
    }

    unsigned char VVec::width() const {
        return width_;
    }

    std::vector<VVal>& VVec::expand_inplace() {
        if (vec_.size() < width_) {
            vec_.resize(width_, defaultval());
        }

        return vec_;
    }

    std::vector<VVal> VVec::expand_copy() const {
        std::vector<VVal> vec = vec_;

        if (vec.size() < width_)
            vec.resize(width_, defaultval());

        return vec;
    }

    const VVal VVec::operator[](unsigned char index) const {
        if (index >= vec_.size())
            return defaultval();

        return vec_[index];
    }

    VVal& VVec::at(unsigned char index) {
        if (index >= width_)
            throw std::invalid_argument("VVec index out of range");

        if (index >= vec_.size()) {
            extend(index + 1);
            return expand_inplace()[index];
        }

        return vec_[index];
    }

    const VVal VVec::defaultval() const {
        if (!vec_.size())
            return VVal::X;

        switch (*vec_.rbegin()) {
            case VVal::ZERO:
            case VVal::ONE:
                return VVal::ZERO;

            case VVal::Z:
            case VVal::X:
            default:
                return *vec_.rbegin();
        }
    }
}
