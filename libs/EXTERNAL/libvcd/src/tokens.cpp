#include "libvcd_tokens.h"

#include<iostream>

namespace libvcd {
    namespace tok {
        int Decimal::val() const {
            return atoi(val_.c_str());
        }

        VVal Scalar::val() const {
            return VVal(val_);
        }

        VVec Binary::val(unsigned int width) const {
            return VVec(val_, width);
        }

        float Real::val() const {
            float f;
            int res = sscanf("%g", val_.c_str(), &f);

            if (!res)
                throw std::runtime_error("Failed to sscanf real value");

            return f;
        }

        value_tok& value_tok::operator=(Real& val) {
            type_ = net_type::REAL;
            real_ = val.val();

            return *this;
        }

        value_tok& value_tok::operator=(Scalar& val) {
            type_ = net_type::SCALAR;
            val_ = val.val();

            return *this;
        }

        value_tok& value_tok::operator=(Binary& val) {
            type_ = net_type::VECTOR;
            vec_ = val;

            return *this;
        }

        void value_tok::checkType(net_type t) const {
            if (type_ != t)
                throw std::runtime_error("value_tok type mismatch");
        }

        value_change::value_change():
            valtok_(), ident_() {
        }

        value_change::value_change(const value_tok& v,
                                    const std::string& netId):
            valtok_(v), ident_(netId) {
        }

        const std::string& value_change::ident() const {
            return ident_;
        }

        const VVal& value_change::scalarVal() const {
            valtok_.checkType(net_type::SCALAR);
            return valtok_.val_;
        }

        VVec value_change::vectorVal(unsigned int width) const {
            valtok_.checkType(net_type::VECTOR);
            return valtok_.vec_.val(width);
        }

        const float& value_change::realVal() const {
            valtok_.checkType(net_type::REAL);
            return valtok_.real_;
        }

        dump::dump() {
        }

        dump::dump(const std::vector<value_change>& vcList,
                     dump_type type):
            type_(type), changes_(vcList) {
        }

        dump_type dump::type() const {
            return type_;
        }

        const std::vector<value_change>& dump::values() const {
            return changes_;
        }

        index::index():
            from_(-1), to_(-1), has_to_(false), missing_(true) {
        }

        index::index(Decimal from, OptionalDecimal to):
            from_(from.val()),
            to_(to.present() ? to.val() : -1),
            has_to_(to.present()),
            missing_(false) {
        }

        index index::missing() {
            return index();
        }

        bool index::present() const {
            return !missing_;
        }

        index_t index::val() const {
            index_t idx;

            idx.from = from_;
            idx.to = to_;
            idx.exists = !missing_;
            idx.has_to = has_to_;

            return idx;
        }

        const char* Decimal::innerval() const {
            return val_.c_str();
        }

        OptionalDecimal::OptionalDecimal():
            missing_(true), Decimal() {
        }

        OptionalDecimal::OptionalDecimal(const char* value):
            missing_(false), Decimal(value) {
        }

        OptionalDecimal OptionalDecimal::missing() {
            return OptionalDecimal();
        }

        OptionalDecimal OptionalDecimal::operator=(const Decimal& decimal) {
            val_ = innerval();
            missing_ = false;

            return *this;
        }

        bool OptionalDecimal::present() const {
            return !missing_;
        }

        trim_string::trim_string():
            str_() {
        }

        trim_string::trim_string(const std::string& str) {
            updatestr(str);
        }

        trim_string& trim_string::operator=(const std::string& str) {
            updatestr(str);

            return *this;
        }

        trim_string::operator std::string&() {
            return str_;
        }

        trim_string::operator const char*() {
            return str_.c_str();
        }

        void trim_string::updatestr(const std::string& str) {
            size_t from = str.find_first_not_of(' ');
            size_t to = str.find_last_not_of(' ');
            str_ = str.substr(from, 1 + to - from);
        }
    }
}
