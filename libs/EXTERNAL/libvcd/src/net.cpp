#include "libvcd_vcd.h"

#include <sstream>

namespace libvcd {
    net::net(const std::string& name,
                var_type varType,
                unsigned char vec_width,
                index_t idx,
                const scope& scope):
        name_(name),
        id_(INVALID),
        ident_("!INVALID!"),
        type_(getNetType(varType, vec_width)),
        vartype_(varType),
        vec_width_(vec_width),
        index_(idx),
        scope_(scope) {
        if (idx.exists) {
            std::stringstream ss;

            ss << name_ << "[" << index_.from;

            if (index_.has_to)
                ss << ":" << index_.to;

            ss << "]";

            full_name_ = ss.str();
        }
    }

    const scope& net::net_scope() const {
        return scope_;
    }

    net::id_t net::id() const {
        return id_;
    }

    void net::setId(id_t id) {
        id_ = id;
    }

    const net::ident_t& net::ident() const {
        return ident_;
    }

    void net::setIdent(const ident_t& ident) {
        ident_ = ident;
    }

    const std::string& net::name() const {
        if (!index_.exists)
            return name_;

        return full_name_;
    }

    net_type net::netType() const {
        return type_;
    }

    var_type net::varType() const {
        return vartype_;
    }

    unsigned char net::vec_width() const {
        return vec_width_;
    }
}
