#include "vtr_logic.h"

#include <sstream>
#include "vtr_assert.h"

namespace vtr {
    std::ostream& operator<<(std::ostream& os, const LogicValue& val) {
        auto lookup = [](const LogicValue& value) {
            switch (value) {
                case LogicValue::FALSE:
                    return "0";

                case LogicValue::TRUE:
                    return "1";

                case LogicValue::DONT_CARE:
                    return "-";

                default:
                    VTR_ASSERT_MSG(false, "Unknown vtr::LogicValue!");
                    // fall-through

                case LogicValue::UNKNOWN:
                    return "?";
            }
        };

        return os << lookup(val);
    }
}
