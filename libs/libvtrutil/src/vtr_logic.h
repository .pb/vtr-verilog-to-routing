#ifndef VTR_LOGIC_H
#define VTR_LOGIC_H

#include <sstream>

namespace vtr {

    enum class LogicValue {
        FALSE = 0,
        TRUE = 1,
        DONT_CARE = 2,
        UNKNOWN = 3
    };

    std::ostream& operator<<(std::ostream& os, const LogicValue& val);
} //namespace

#endif
