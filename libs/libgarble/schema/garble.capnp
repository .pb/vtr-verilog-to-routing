@0xfcaaefffc70eec597301;

# Protocol definition for GARBLE, the Gate ARray Bytecode LanguagE.
#
# @author Peter H. A. Bridgman <phb12@ic.ac.uk>

using Cxx = import "/capnp/c++.capnp";
$Cxx.namespace("garble");

struct Executable {
    # GARBLE Executable

    dSpec @0 :DataSpaceSpec;
    dInit @1 :List(DataSpaceInit);
    iSegs @2 :List(InstructionSegment);
}

struct InstructionSegment {
    # Represents a named set of instructions that can be executed as a unit.

    name @0 :Text;
    # The name of the segment.

    iList @1 :List(Instruction);
    # The set of instructions in the segment.
}

struct NetMap {
    # Mapping between a data-space address and a net name.

    addr @0 :UInt32;
    # The data-space address of the net being named.

    name @1 :Text;
    # The name of the net.
}

struct DataSpaceSpec {
    # Specifies the layout of the data address spaces.

    netCount @0 :UInt32;
    # How many nets are there in the data space?

    namedNetMap @1 :List(NetMap);
    # Mapping of data-space addresses to net names.

    omegaBarrier @2 :UInt32;
    # Address of the highest permissible reference into the omega space.
}

struct NetState {
    # Possible values for the state of a net.

    union {
        zero @0 :Void;
        one @1 :Void;
        open @2 :Void;
        x @3 :Void;
    }
}

struct DataSpaceInit {
    # Declarator for how a region of data-space addresses should be
    # initialised at the beginning of the simulation.

    addr @0 :UInt32;
    # Initial data-space address of the initialisation block.

    count @1 :UInt32;
    # Number of addresses to be initialised.

    val @2 :NetState;
    # Value to which the specified addresses should be initialised.
}

struct Instruction {
    # A single instruction in the program.

    destD @0 :UInt32;
    # The address in the delta data space into which the result should be written.

    op :union {
        # The operation to be performed.

        movO @1 :InstrMovOmega;
        lut @2 :InstrLut;
    }
}

struct InstrMovOmega {
    # Move a net value from a specified address in the omega space
    # into the delta space.

    src @0 :UInt32;
    # The address in the omega space from which the value should be read.
}

struct InstrLut {
    # Perform a LUT evaluation for a k-LUT

    srcDs @0 :List(UInt32);
    # The addresses from which the inputs to the LUT should be read.

    mask @1 :List(UInt64);
    # The configuration bitmask for the LUT, little endian, LSB.
}
